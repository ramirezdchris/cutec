package com.example.clinicautec.Modelo;

public class Usuario {

    private String usuario;
    private String pass;
    private Personal personal;
    private Rol rol;

    public Usuario() {
    }

    public Usuario(String usuario, String pass, Personal personal, Rol rol) {
        this.usuario = usuario;
        this.pass = pass;
        this.personal = personal;
        this.rol = rol;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "usuario='" + usuario + '\'' +
                ", pass='" + pass + '\'' +
                ", personal=" + personal +
                ", rol=" + rol +
                '}';
    }
}
