package com.example.clinicautec.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Util.ConexionSQLiteHelper;

import java.util.ArrayList;

public class PacienteDao {
    ConexionSQLiteHelper conn;
    SQLiteDatabase db;

    public PacienteDao(Context context){
        conn = new ConexionSQLiteHelper(context, "db", null, 1);

    }

    public void openDB(){
        db = conn.getWritableDatabase(); // Abrimos la base de datos para editarla
    }

    public void closeDB(){
        conn.close();
        db.close();
    }

    public ArrayList<Paciente> getPacientes(){
        ArrayList<Paciente> listaPacientes = new ArrayList<>();
        try {
            Cursor c = conn.getData("SELECT * FROM paciente;");

            while(c.moveToNext()){
                Expediente expediente = new Expediente();

                Paciente paciente = new Paciente();

                paciente.setIdPaciente(c.getInt(0));
                paciente.setNombre(c.getString(1));
                paciente.setApellido(c.getString(2));
                paciente.setDui(c.getString(3));
                paciente.setTelefono(c.getString(4));
                paciente.setFechaNacimiento(c.getString(5));
                listaPacientes.add(paciente);
            }
            return listaPacientes;
        }catch (Exception e){
            System.err.println("Error Metodo getPersonal CitasDao" +e.getMessage());
            return null;
        }
    }
}
