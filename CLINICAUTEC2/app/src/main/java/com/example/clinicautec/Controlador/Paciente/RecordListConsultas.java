package com.example.clinicautec.Controlador.Paciente;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clinicautec.Controlador.Doctor.RecordListHistorial;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Consulta;
import com.example.clinicautec.R;

import java.util.ArrayList;

public class RecordListConsultas extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Consulta> recordListCitas;

    public RecordListConsultas (Context context, int layout,ArrayList<Consulta> recordListCitas){

        this.context = context;
        this.layout = layout;
        this.recordListCitas = recordListCitas;
    }

    @Override
    public int getCount() {
        return recordListCitas.size();
    }

    @Override
    public Object getItem(int i) {
        return recordListCitas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    public class VistaRowHistorial{
        TextView tvfechadoctor, tvhoradoctor, tvnombredoctor, tvexpedientedoctor;
        Button btnacciondoctorlista;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View row = view;
        VistaRowHistorial holder2 = new VistaRowHistorial();

        if(row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder2.tvfechadoctor = row.findViewById(R.id.tvfechadoctor);
            holder2.tvhoradoctor = row.findViewById(R.id.tvhoradoctor);
            holder2.tvnombredoctor = row.findViewById(R.id.tvnombredoctor);
            holder2.tvexpedientedoctor = row.findViewById(R.id.tvexpedientedoctor);
            holder2.btnacciondoctorlista = row.findViewById(R.id.btnacciondoctorlista);
            row.setTag(holder2);
        }
        else {
            holder2 = (RecordListConsultas.VistaRowHistorial) row.getTag();
        }

        Consulta consulta = recordListCitas.get(i);

        final int Expediente = consulta.getIdConsulta();
        String nombre = consulta.getCita().getPersonal().getNombre();
        String apellido = consulta.getCita().getPersonal().getApellido();
        holder2.tvfechadoctor.setText(consulta.getDia());
        holder2.tvhoradoctor.setText(consulta.getHora());
        holder2.tvnombredoctor.setText(nombre+" "+apellido);
        holder2.tvexpedientedoctor.setText(consulta.getEstado());
        holder2.btnacciondoctorlista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogoBasico(Expediente);
            }
        });

        return row;
    }

    private void mostrarDialogoBasico(final int idconsulta){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        final String idexpediente = String.valueOf(idconsulta);
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this.context);
        dialogo1.setTitle("VER EXPEDIENTE DE CONSULTA");
        dialogo1.setMessage("¿Desea ver este expediente  ");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar(idexpediente);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialogo1.create().show();
    }

    public void aceptar(String idexpediente) {
        Intent objExpediente = new Intent(context, VerConsultaPaciente.class);
        objExpediente.putExtra("IDconsulta", idexpediente);
        context.startActivity(objExpediente);
        Toast t=Toast.makeText(context,"Ver Consulta numero "+idexpediente, Toast.LENGTH_SHORT);
        t.show();
    }

    public void cancelar() {
        Toast t=Toast.makeText(context,"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }
}
