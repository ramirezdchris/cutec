package com.example.clinicautec.Modelo;

public class Personal {

    private int idPersonal;
    private String nombre, apellido, dui, correo, telefono;

    public Personal() {
    }

    public Personal(int idPersonal, String nombre, String apellido, String dui, String correo, String telefono) {
        this.idPersonal = idPersonal;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dui = dui;
        this.correo = correo;
        this.telefono = telefono;
    }

    public Personal(String nombre, String apellido, String dui, String correo, String telefono) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dui = dui;
        this.correo = correo;
        this.telefono = telefono;
    }

    public int getIdPersonal() {
        return idPersonal;
    }

    public void setIdPersonal(int idPersonal) {
        this.idPersonal = idPersonal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public String toString() {
        return "Personal{" +
                "idPersonal=" + idPersonal +
                ", nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", dui='" + dui + '\'' +
                ", correo='" + correo + '\'' +
                ", telefono='" + telefono + '\'' +
                '}';
    }
}
