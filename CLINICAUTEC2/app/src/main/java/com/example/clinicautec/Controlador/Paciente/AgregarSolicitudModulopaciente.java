package com.example.clinicautec.Controlador.Paciente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Solicitudes;
import com.example.clinicautec.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;

public class AgregarSolicitudModulopaciente extends AppCompatActivity {

    EditText edtsintomapaciente,edtfechaspaciente,edthorapaciente;
    Button btnenviarpaciente,btnactualizarpaciente;

    RequestQueue queue;
    Solicitudes solicitud = new Solicitudes();
    Expediente expediente = new Expediente();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_solicitud_modulopaciente);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        queue = Volley.newRequestQueue(getApplicationContext());

        edtsintomapaciente = findViewById(R.id.edtsintomapaciente);
        edtfechaspaciente = findViewById(R.id.edtfechaspaciente);
        edthorapaciente = findViewById(R.id.edthorapaciente);
        btnenviarpaciente = findViewById(R.id.btnenviarpaciente);
        btnactualizarpaciente = findViewById(R.id.btnactualizarpaciente);

        final String User = getIntent().getStringExtra("idExpediente");

        //Datos Para Actualizar
        final String IdSolicitud = getIntent().getStringExtra("IdSolicitud");
        final String fechaActualizar = getIntent().getStringExtra("fechaActualizar");
        final String horaActualizar = getIntent().getStringExtra("horaActualizar");
        final String sintomaActualizar = getIntent().getStringExtra("sintomaActualizar");

        if(TextUtils.isEmpty(IdSolicitud)){
            btnenviarpaciente.setVisibility(View.VISIBLE);
            btnactualizarpaciente.setVisibility(View.GONE);
        }else{
            btnenviarpaciente.setVisibility(View.GONE);
            btnactualizarpaciente.setVisibility(View.VISIBLE);
            edtsintomapaciente.setText(sintomaActualizar);
            edtfechaspaciente.setText(fechaActualizar);
            edthorapaciente.setText(horaActualizar);

        }

        edtfechaspaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                final int mYear = c.get(Calendar.YEAR);
                final int mMonth = c.get(Calendar.MONTH);
                final int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AgregarSolicitudModulopaciente.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        final int mesActual = month + 1;
                        String diaFormateado = (dayOfMonth < 10) ? "0" +String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                        String mesFormateado = (mesActual < 10) ? "0" +String.valueOf(mesActual) : String.valueOf(mesActual);

                            edtfechaspaciente.setText(year + "-" +mesFormateado +"-" +diaFormateado);

                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        edthorapaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(AgregarSolicitudModulopaciente.this,R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String horaFormateada = (hourOfDay < 10) ? String.valueOf("0" + hourOfDay) : String.valueOf(hourOfDay);
                        String minutoFormateado = (minute < 10) ? String.valueOf("0" + minute) : String.valueOf(minute);
                        if(minutoFormateado.equals("00") || minutoFormateado.equals("30")){
                            edthorapaciente.setText(horaFormateada +":" +minutoFormateado);
                            edthorapaciente.setTextColor(Color.parseColor("#28A530"));
                        }else{
                            //edtHora.setError("Solo intervalos de 00 y 30");
                            edthorapaciente.setText("Hora debe ser en punto o media hora");
                            edthorapaciente.setTextColor(Color.parseColor("#CF0707"));
                            //edtHora.setTextColor(Color.parseColor("#F08686"));
                        }

                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        btnenviarpaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sintoma = edtsintomapaciente.getText().toString().trim();
                String fecha =edtfechaspaciente.getText().toString().trim();
                String hora = edthorapaciente.getText().toString().trim();
                if(TextUtils.isEmpty(sintoma)){
                    edtsintomapaciente.setError("Este campo no puede quedar Vacio");
                    edtsintomapaciente.requestFocus();
                }else if(TextUtils.isEmpty(fecha)){
                    edtfechaspaciente.setError("Este campo no puede quedar Vacio");
                    edtfechaspaciente.requestFocus();
                }else if(TextUtils.isEmpty(hora)){
                    edthorapaciente.setError("Este campo no puede quedar Vacio");
                    edthorapaciente.requestFocus();
                }else{

                    if(hora.equals("Hora debe ser en punto o media hora")){
                        edthorapaciente.setText("Campo no puede ir vacio");
                        edthorapaciente.setTextColor(Color.parseColor("#CF0707"));
                    }else{
                        solicitud.setDia(edtfechaspaciente.getText().toString().trim());
                        solicitud.setHora(edthorapaciente.getText().toString().trim());
                        solicitud.setSintoma(edtsintomapaciente.getText().toString().trim());
                        expediente.setIdExpediente(User);
                        solicitud.setExpediente(expediente);
                        mostrarDialogoBasico(solicitud);

                    }

                }
            }
        });

        btnactualizarpaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sintoma = edtsintomapaciente.getText().toString().trim();
                String fecha =edtfechaspaciente.getText().toString().trim();
                String hora = edthorapaciente.getText().toString().trim();
                if(TextUtils.isEmpty(sintoma)){
                    edtsintomapaciente.setError("Este campo no puede quedar Vacio");
                    edtsintomapaciente.requestFocus();
                }else if(TextUtils.isEmpty(fecha)){
                    edtfechaspaciente.setError("Este campo no puede quedar Vacio");
                    edtfechaspaciente.requestFocus();
                }else if(TextUtils.isEmpty(hora)){
                    edthorapaciente.setError("Este campo no puede quedar Vacio");
                    edthorapaciente.requestFocus();
                }else{

                    if(hora.equals("Hora debe ser en punto o media hora")){
                        edthorapaciente.setText("Campo no puede ir vacio");
                        edthorapaciente.setTextColor(Color.parseColor("#CF0707"));
                    }else{
                        solicitud.setIdSolicitud(Integer.parseInt(IdSolicitud));
                        solicitud.setDia(edtfechaspaciente.getText().toString().trim());
                        solicitud.setHora(edthorapaciente.getText().toString().trim());
                        solicitud.setSintoma(edtsintomapaciente.getText().toString().trim());
                        expediente.setIdExpediente(User);
                        solicitud.setExpediente(expediente);
                        ActualizarSolicitud(solicitud);

                    }

                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void mostrarDialogoBasico(final Solicitudes solicitudes){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(AgregarSolicitudModulopaciente.this);
        dialogo1.setTitle("SOLICITUD LLENA");
        dialogo1.setMessage("¿Desea enviar esta solicitud ");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar(solicitudes);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialogo1.create().show();
    }

    public void aceptar(final Solicitudes solicitudes) {
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/solicitudes/InsertarSolicitudPaciente.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("dia", solicitudes.getDia());
            jsonObject.put("hora", solicitudes.getHora());
            jsonObject.put("sintoma", solicitudes.getSintoma());
            jsonObject.put("mensaje", "Pendiente");
            jsonObject.put("estado", "Pendiente");
            jsonObject.put("id_expediente", solicitudes.getExpediente().getIdExpediente());

            edtsintomapaciente.setText("");
            edtfechaspaciente.setText("");
            edthorapaciente.setText("");

            //Toast.makeText(getApplicationContext(), "Datos Correctamente Insertados", Toast.LENGTH_LONG).show();

        }catch (JSONException e){
            System.out.println("ERROR AL INSETAR LOS DATOS: " +e.getMessage());
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Datos Correctamente Insertados", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);

    }

    public void cancelar() {
        Toast t=Toast.makeText(getApplicationContext(),"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }

    private void ActualizarSolicitud(final Solicitudes solicitudes){


        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(AgregarSolicitudModulopaciente.this);
        dialogo1.setTitle("ACTUALIZAR SOLICITUD");
        dialogo1.setMessage("¿Desea actualizar esta solicitud ");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptarActualizar(solicitudes);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelarActulizacion();
            }
        });
        dialogo1.create().show();
    }

    public void aceptarActualizar(final Solicitudes solicitudes) {
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/solicitudes/ActualizarSolicitudPaciente.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("id_solicitud", solicitudes.getIdSolicitud());
            jsonObject.put("dia", solicitudes.getDia());
            jsonObject.put("hora", solicitudes.getHora());
            jsonObject.put("estado", "Pendiente");
            jsonObject.put("sintoma", solicitudes.getSintoma());
            jsonObject.put("id_expediente", solicitudes.getExpediente().getIdExpediente());



            //Toast.makeText(getApplicationContext(), "Datos Correctamente Insertados", Toast.LENGTH_LONG).show();

        }catch (JSONException e){
            System.out.println("ERROR AL INSETAR LOS DATOS: " +e.getMessage());
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Datos Actualizados Correctamente", Toast.LENGTH_LONG).show();
                Intent objAgregarCita = new Intent(getApplicationContext(), HistorialSolicitudesPaciente.class);
                objAgregarCita.putExtra("idExpediente", solicitudes.getExpediente().getIdExpediente());
                startActivity(objAgregarCita);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);

    }

    public void cancelarActulizacion() {
        Toast t=Toast.makeText(getApplicationContext(),"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }
}
