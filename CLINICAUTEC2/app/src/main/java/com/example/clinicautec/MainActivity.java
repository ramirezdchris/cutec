package com.example.clinicautec;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Administrador.MenuAdministrador;
import com.example.clinicautec.Controlador.Doctor.MenuDoctor;
import com.example.clinicautec.Controlador.Enfermera.MenuEnfermera;
import com.example.clinicautec.Controlador.Paciente.MenuPaciente;
import com.example.clinicautec.DAO.UsuarioDao;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.Modelo.Rol;
import com.example.clinicautec.Modelo.Usuario;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener{

    EditText edtUsuario, edtClave;
    Button btningresar;
    TextView tvMensaje;

    UsuarioDao userD;

    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;

    Usuario u = new Usuario();
    Rol rol = new Rol();
    Personal personal = new Personal();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Ocultar barra de notificaciones
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //  Ocultar barra ActionBar(header)
        //getSupportActionBar().hide();

        edtUsuario = findViewById(R.id.edtUsuario);
        edtClave = findViewById(R.id.edtClave);
        btningresar = findViewById(R.id.btnIngresar);
        tvMensaje = findViewById(R.id.tvMensaje);
        userD = new UsuarioDao(getApplicationContext());



        queue = Volley.newRequestQueue(getApplicationContext());

        edtUsuario.setText("doris");
        edtClave.setText("123");
        tvMensaje.setVisibility(View.INVISIBLE);
        tvMensaje.setPaintFlags(tvMensaje.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        btningresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user = edtUsuario.getText().toString().trim();
                String pass = edtClave.getText().toString().trim();
                //loginWS(user, pass);

                if(TextUtils.isEmpty(user)){
                    edtUsuario.setError("Digite su usuario");
                    edtUsuario.requestFocus();
                }else if(TextUtils.isEmpty(pass)){
                    edtClave.setError("Digite su clave");
                    edtClave.requestFocus();
                }else{
                    loginWS(user, pass);

                }

              

            }
        });

        tvMensaje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), "hola" , Toast.LENGTH_SHORT).show();
                Intent objRecuperar = new Intent(getApplicationContext(), RecuperarClave.class);
                startActivity(objRecuperar);
            }
        });

    }

    public void loginWS(String user, String pass){
        //String url = "http://192.168.1.15:8080/webservice/WebService/API/usuario/login.php?user="+user +"&pass="+pass;


        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/usuario/login.php?user="+user+"&pass="+pass;

        //String url = "http://192.168.1.10:8080/WEBSERVICE2/webservice/WebService/API/usuario/login.php?user="+user+"&pass="+pass;


        //String url = "http://192.168.1.11:8080/WEBSERVICE2/webservice/WebService/API/usuario/login.php?user="+user+"&pass="+pass;


        //url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        queue.add(jsonObjectRequest);

    }

    @Override
    public void onResponse(JSONObject response) {
        //Toast.makeText(getApplicationContext(), "Mensaje: " +response, Toast.LENGTH_SHORT).show();
        //Usuario u = new Usuario();
        //Rol rol = new Rol();
        //Personal personal = new Personal();

        try {
            //JSONObject jsonObject = response.getJSONObject("rol");
            System.out.println("AQUIIIIIIIIIIIIII PEROOOOOOOOOOOOOOOOOO" +response.getInt("rol"));

            String id = response.getString("id_usuario");
            String nombre = response.getString("nombre");
            String apellido = response.getString("apellido");
            int rol = response.getInt("rol");

            System.out.println("ID de Usuario: " +id);
            System.out.println("Nombre de Usuario: " +nombre);
            System.out.println("Apellido de Usuario: " +apellido);
            System.out.println("Rol de Usuario: " +rol);

            SharedPreferences usuarioIniciado = getSharedPreferences("session", Context.MODE_PRIVATE);
            SharedPreferences.Editor editarMisPreferencias = usuarioIniciado.edit();

            editarMisPreferencias.putString("id_usuario", id);
            editarMisPreferencias.putString("nombre", nombre);
            editarMisPreferencias.putString("apellido", apellido);
            editarMisPreferencias.putInt("rol", rol);
            editarMisPreferencias.commit();

            if (rol == 0) {
                Toast.makeText(getApplicationContext(), "Credenciales Invalidas", Toast.LENGTH_SHORT).show();
                tvMensaje.setVisibility(View.VISIBLE);
            } else if (rol == 4) {
                Intent objPaciente = new Intent(getApplicationContext(), MenuPaciente.class);
                startActivity(objPaciente);
            } else if (rol == 2) {
                Intent objEnfemera = new Intent(getApplicationContext(), MenuEnfermera.class);
                startActivity(objEnfemera);
            } else if (rol == 3) {
                Intent objDoctor = new Intent(getApplicationContext(), MenuDoctor.class);
                startActivity(objDoctor);
            } else if(rol == 1){
                //Toast.makeText(getApplicationContext(), "Bienvenido admisnitrador", Toast.LENGTH_SHORT).show();
                Intent objDoctor = new Intent(getApplicationContext(), MenuAdministrador.class);
                startActivity(objDoctor);
            }else{
                Toast.makeText(getApplicationContext(), "Credenciales Invalidas", Toast.LENGTH_SHORT).show();
                tvMensaje.setVisibility(View.VISIBLE);
            }

        }catch (JSONException e){
            System.out.println("Error Catch: " +e.getMessage());
        }


    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "No se puede establecer conexion con el servidor", Toast.LENGTH_SHORT).show();
        System.out.println("Error Response: " +error.getMessage());
    }


}
