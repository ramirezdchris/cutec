package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.DAO.CitasDao;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaPacientes extends AppCompatActivity {


    Expediente expediente;
    Paciente paciente;


    FloatingActionButton flbuton;
    TableLayout tblListaCita;

    ListView lv;
    ArrayList<Expediente> listaExpediente;
    ArrayList<String> lista;
    RecordListPacientes adaptador = null;

    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pacientes);
        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //btnEnfeActualizar = findViewById(R.id.btnEnfeActualizar);
        //btnEnfeEliminar = findViewById(R.id.btnEnfeEliminar);

        queue = Volley.newRequestQueue(getApplicationContext());

        expediente = new Expediente();
        paciente = new Paciente();


        lv = (ListView)findViewById(R.id.lista);





        listaPacientes();

        flbuton = findViewById(R.id.floatingActionButton);
        flbuton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objAgregarPaciente = new Intent(getApplicationContext(), AgregarPacientes.class);
                objAgregarPaciente.putExtra("actualizar", "no");
                startActivity(objAgregarPaciente);
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void listaPacientes(){
        //String url = "http://192.168.1.15:8080/webservice/WebService/API/cita/read.php";
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/expediente/read_expedientes.php";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                lista = new ArrayList<>();
                listaExpediente = new ArrayList<>();

                try {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for(int i=0; i < mJsonArray.length(); i++){
                        JSONObject mJSONObject = mJsonArray.getJSONObject(i);


                        expediente = new Expediente();
                        paciente = new Paciente();

                        expediente.setIdExpediente(mJSONObject.getString("id_expediente"));
                        paciente.setIdPaciente(mJSONObject.getInt("id_paciente"));
                        paciente.setNombre(mJSONObject.getString("nombre"));
                        paciente.setApellido(mJSONObject.getString("apellido"));
                        paciente.setDui(mJSONObject.getString("dui"));
                        paciente.setCarnet(mJSONObject.getString("carnet"));
                        paciente.setTelefono(mJSONObject.getString("telefono"));
                        paciente.setFechaNacimiento(mJSONObject.getString("fecha_nacimiento"));
                        paciente.setCorreo(mJSONObject.getString("correo"));
                        paciente.setPass(mJSONObject.getString("pass"));


                        expediente.setPaciente(paciente);

                        listaExpediente.add(expediente);

                    }
                }catch (JSONException e){
                    e.printStackTrace();
                    System.out.println("Error CATCH Lista Cita: " +e.getMessage());
                }

                adaptador = new RecordListPacientes(ListaPacientes.this, R.layout.activity_row_pacientes, listaExpediente);
                lv.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error Lista Pacientes: " +error.getMessage());
                Toast.makeText(getApplicationContext(), "Error" +error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        queue.add(request);
    }

    /*public void mostrarDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(ListaPacientes.this);
        LayoutInflater inflater = getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_rechazar_solicitud, null);

        builder.setView(view);

        final AlertDialog dialog = builder.create();
        dialog.show();

        Button btnActualizar = view.findViewById(R.id.btnActualizarPaciente);
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }*/
}
