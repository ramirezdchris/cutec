package com.example.clinicautec.Modelo;

public class Consulta {

    private int idConsulta;
    private String dia;
    private String hora;
    private String diagnostico;
    private String tratamiento;
    private String estado;
    private Cita cita;

    public Consulta() {
    }

    public Consulta(int idConsulta, String dia, String hora, String diagnostico,String tratamiento,String estado ,Cita cita) {
        this.idConsulta = idConsulta;
        this.dia = dia;
        this.hora = hora;
        this.diagnostico = diagnostico;
        this.tratamiento = tratamiento;
        this.estado = estado;
        this.cita = cita;
    }

    public Consulta(String dia, String hora, String diagnostico, Cita cita) {
        this.dia = dia;
        this.hora = hora;
        this.diagnostico = diagnostico;
        this.cita = cita;
    }

    public int getIdConsulta() {
        return idConsulta;
    }

    public void setIdConsulta(int idConsulta) {
        this.idConsulta = idConsulta;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public Cita getCita() {
        return cita;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public void setCita(Cita cita) {
        this.cita = cita;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Consulta{" +
                "idConsulta=" + idConsulta +
                ", dia='" + dia + '\'' +
                ", hora='" + hora + '\'' +
                ", diagnostico='" + diagnostico + '\'' +
                ", cita=" + cita +
                '}';
    }
}
