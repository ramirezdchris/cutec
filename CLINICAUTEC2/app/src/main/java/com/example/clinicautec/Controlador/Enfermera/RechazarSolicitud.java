package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.R;

import org.json.JSONException;
import org.json.JSONObject;

public class RechazarSolicitud extends AppCompatActivity {

    Button btnRechazarSoli;
    EditText edtMotivoSoli;
    TextView tvRechazarSoli;


    int id_solicitud;
    String id_expediente;

    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechazar_solicitud);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        edtMotivoSoli = findViewById(R.id.edtMotivoSoli);
        tvRechazarSoli = findViewById(R.id.tvRechazarSolicitud);
        btnRechazarSoli = findViewById(R.id.btnRechazarSoli);

        id_solicitud = getIntent().getIntExtra("id_solicitud" , 0);
        id_expediente = getIntent().getStringExtra("id_expediente");
        tvRechazarSoli.setText("Esta rechazando la solicitud de cita del expediente: " +id_expediente);

        btnRechazarSoli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String motivo = edtMotivoSoli.getText().toString().trim();

                if(TextUtils.isEmpty(motivo)){
                    edtMotivoSoli.setError("Porfavor completar el campo");
                }else{
                    rechazarSoli(id_solicitud, id_expediente, motivo);
                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void rechazarSoli(int id_solicitud, String id_expediente, String motivo){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/solicitudes/actualizarSolicitudEnfermera.php";
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject();
            jsonObject.put("id_solicitud", id_solicitud);
            jsonObject.put("estado", "Rechazado");
            jsonObject.put("mensaje" , motivo);
        }catch (JSONException e){
            System.out.println("Error en el CATCH RechazarSolicitud: " +e.getMessage());
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Solicitud rechazada" , Toast.LENGTH_SHORT).show();
                Intent objEnfemera = new Intent(getApplicationContext(), SolicitudesPacientes.class);
                startActivity(objEnfemera);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        queue.add(request);
    }
}
