package com.example.clinicautec.Controlador.Paciente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Consulta;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistorialConsultaPaciente extends AppCompatActivity {

    ListView lv;
    ArrayList<Consulta> lista;
    ArrayList<String> lista2;
    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;
    RecordListConsultas adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_consulta_paciente);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lv = findViewById(R.id.lvhistorialPacienteC);
        queue = Volley.newRequestQueue(getApplicationContext());

        String User = getIntent().getStringExtra("idExpediente");

        HistorialPAciente(User);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void HistorialPAciente(String idexpediente){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/consulta/HistorialPaciente.php?id="+idexpediente;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                lista2 = new ArrayList<String>();
                lista = new ArrayList<>();
                try {
                    JSONArray json=response.getJSONArray("data");
                    for (int i=0;i<json.length();i++){
                        Cita c = new Cita();
                        Expediente e = new Expediente();
                        Personal p =new Personal();
                        Consulta con = new Consulta();
                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(i);
                        con.setIdConsulta(jsonObject.optInt("id_consulta"));
                        con.setDia(jsonObject.optString("dia"));
                        con.setHora(jsonObject.optString("hora"));
                        con.setEstado(jsonObject.optString("estado"));
                        p.setNombre(jsonObject.optString("nombre"));
                        p.setApellido(jsonObject.optString("apellido"));
                        e.setIdExpediente(jsonObject.optString("Expediente"));
                        c.setPersonal(p);
                        c.setExpediente(e);
                        con.setCita(c);

                        lista.add(con);
                        //LLenar  el jsonObject para llenar la lista
                        lista2.add(jsonObject.getString("id_consulta") +" " +jsonObject.getString("dia") + " " +jsonObject.getString("hora")
                                + " " +jsonObject.getString("estado")+ " " +jsonObject.getString("nombre")+ " " +jsonObject.getString("apellido")
                                + " " +jsonObject.getString("Expediente"));

                        adaptador = new RecordListConsultas(HistorialConsultaPaciente.this, R.layout.activity_row_historial_m_paciente, lista);
                        lv.setAdapter(adaptador);
                        adaptador.notifyDataSetChanged();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No hay ninguna Consulta por el Momento", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }
}
