package com.example.clinicautec.Controlador.Doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.clinicautec.MainActivity;
import com.example.clinicautec.R;

public class MenuDoctor extends AppCompatActivity {

    CardView btnlistapaciente, btnlistacita;
    String opcion1= "atender",opcion2= "ver";
    String RolUser;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_doctor);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        btnlistapaciente = findViewById(R.id.btnlistapaciente2);
        btnlistacita = findViewById(R.id.btnlistacita2);

        session = getSharedPreferences("session", Context.MODE_PRIVATE);
        String id = session.getString("id", "");
        String nombre = session.getString("nombre", "");
        String apellido = session.getString("apellido", "");
        int rol = session.getInt("rol", 0);
        Toast.makeText(getApplicationContext(), "Nombre " +nombre +" ID: " +rol, Toast.LENGTH_LONG).show();

        RolUser = String.valueOf(rol);
        btnlistacita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Lista_CitasDoctor.class);
                i.putExtra("RolUser", RolUser);
                i.putExtra("dato", opcion1);
                startActivity(i);
            }
        });

        btnlistapaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ListaPacientesAsignadosD.class);
                i.putExtra("RolUser", RolUser);
                i.putExtra("dato", opcion2);
                startActivity(i);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
