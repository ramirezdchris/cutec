package com.example.clinicautec.Controlador.Doctor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.R;

import java.util.ArrayList;


public class RecordListPaciente extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Cita> recordListUsu;
    private String opcion;


    public RecordListPaciente(Context context, int layout, ArrayList<Cita> recordListUsu,String opcion){
        this.context = context;
        this.layout = layout;
        this.recordListUsu = recordListUsu;
        this.opcion=opcion;
    }

    @Override
    public int getCount() {
        return recordListUsu.size();
    }

    @Override
    public Object getItem(int i) {
        return recordListUsu.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView txtCodigo, txtNombreCom, txtUsuario, txtCorreo, txtTipo;
        Button btnacciondoctor,btnverdoctor;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View row = view;
        ViewHolder holder = new ViewHolder();
        if(row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.txtCodigo = row.findViewById(R.id.tvCodigoRowUsu);
            holder.txtNombreCom = row.findViewById(R.id.tvNombreComUsu);
            holder.txtUsuario = row.findViewById(R.id.tvUsuarioRowUsu);
            holder.txtCorreo = row.findViewById(R.id.tvEmailRowUsu);
            holder.btnacciondoctor = row.findViewById(R.id.btnacciondoctor);
            holder.btnverdoctor = row.findViewById(R.id.btnverdoctor);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder)row.getTag();
        }

        if(opcion.equals("atender")){
            Cita model = recordListUsu.get(i);
            final String nombre = String.valueOf(model.getExpediente().getPaciente().getNombre());
            final String  expediente = String.valueOf(model.getExpediente().getIdExpediente());
            final  int idcita = model.getIdCita();

            holder.txtCodigo.setText(String.valueOf(model.getExpediente().getIdExpediente()));
            holder.txtNombreCom.setText(model.getExpediente().getPaciente().getNombre());
            holder.txtUsuario.setText(model.getExpediente().getPaciente().getTelefono());
            holder.txtCorreo.setText(model.getExpediente().getPaciente().getCorreo());
            holder.btnacciondoctor.setVisibility(View.VISIBLE);
            holder.btnverdoctor.setVisibility(View.GONE);

            holder.btnacciondoctor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mostrarDialogoBasico(expediente,nombre,idcita);
                    //Toast.makeText(context.getApplicationContext(),"Observar Expediente...  "+expediente,Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            Cita model = recordListUsu.get(i);
            final String nombre = String.valueOf(model.getExpediente().getPaciente().getNombre());
            final String  expediente = String.valueOf(model.getExpediente().getIdExpediente());

            holder.txtCodigo.setText(String.valueOf(model.getExpediente().getIdExpediente()));
            holder.txtNombreCom.setText(model.getExpediente().getPaciente().getNombre());
            holder.txtUsuario.setText(model.getExpediente().getPaciente().getTelefono());
            holder.txtCorreo.setText(model.getExpediente().getPaciente().getCorreo());
            holder.btnacciondoctor.setVisibility(View.GONE);
            holder.btnverdoctor.setVisibility(View.VISIBLE);

            holder.btnverdoctor.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mostrarDialogoBasico2(expediente,nombre);

                }
            });
        }

        return row;
    }

    private void mostrarDialogoBasico(final String expediente, String nombre, final int Idcita){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this.context);
        dialogo1.setTitle("CONSULTA");
        dialogo1.setMessage("¿Desea atender al Paciente  "+nombre+" ?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar1(expediente,Idcita);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialogo1.create().show();
    }

    public void aceptar1(String expediente, int Idcita) {
        String exp = String.valueOf(expediente);
        String idcita = String.valueOf(Idcita);
        Intent objExpediente = new Intent(context, ExpedienteDoctor.class);
        objExpediente.putExtra("carnet", exp);
        objExpediente.putExtra("idcita", idcita);
        context.startActivity(objExpediente);
        //Toast.makeText(context.getApplicationContext(),"Observar Expediente...  "+expediente,Toast.LENGTH_SHORT).show();
    }



    private void mostrarDialogoBasico2(final String expe, String nombre){

        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(context);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("¿ Desea ver este Expediente "+expe+" ?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar(expe);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialogo1.create().show();
    }

    public void aceptar(String expe2) {

        String exp = String.valueOf(expe2);
        Intent objExpediente = new Intent(context, HistrorialConsulataDotor.class);
        objExpediente.putExtra("carnet2", exp);
        context.startActivity(objExpediente);
        //Toast t=Toast.makeText(context,"Bienvenido a probar el programa.", Toast.LENGTH_SHORT);
       // t.show();
    }

    public void cancelar() {
        Toast t=Toast.makeText(context,"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }



}
