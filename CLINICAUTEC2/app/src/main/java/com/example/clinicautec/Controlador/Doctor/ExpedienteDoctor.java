package com.example.clinicautec.Controlador.Doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Consulta;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Map;

public class ExpedienteDoctor extends AppCompatActivity {

    EditText txvdiagnosticodoctor,txvtratamientodoctor;
    TextView txvcarnetexpe,tvfechaHistorial2,tvhoraHistorial2,txvIDcon;

    Button btnterminarconsulta;

    Consulta consulta;
    RequestQueue queue;
    int IDconsulta;

    int hora=0;
    int minutos=0;
    String fechaAct="";
    boolean actualizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expediente_doctor);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());
        consulta = new Consulta();

        Calendar calendario = Calendar.getInstance();
        SimpleDateFormat fecha = new SimpleDateFormat("YYYY-MM-dd");
        fechaAct = fecha.format(calendario.getTime());
        hora = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);

        btnterminarconsulta = (Button)findViewById(R.id.btnterminarHis2);
        txvdiagnosticodoctor = (EditText)findViewById(R.id.txvdiagnosticodoctor);
        txvtratamientodoctor = (EditText)findViewById(R.id.txvhHistorialtratamiento2);
        txvcarnetexpe = (TextView)findViewById(R.id.txvcarnetexpeHis2);

        tvfechaHistorial2 = (TextView)findViewById(R.id.tvfechaHistorial2);
        tvhoraHistorial2 = (TextView)findViewById(R.id.tvhoraHistorial2);

        //LLeno el Expediente
        String carnet = getIntent().getStringExtra("carnet");
        txvcarnetexpe.setText(""+carnet);

        //LLenar el id de Consulta
         final String IDconsul = getIntent().getStringExtra("idcita");
        IDconsulta = Integer.parseInt(IDconsul);
        //txvcarnetexpe.setText(""+carnet);

        //Lleno la hora y fecha automaticamente

        tvfechaHistorial2.setText(""+fechaAct);
        tvhoraHistorial2.setText(+hora+":"+minutos);

        btnterminarconsulta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String diagnostico,tratamiento;

                diagnostico = txvdiagnosticodoctor.getText().toString().trim();
                tratamiento = txvtratamientodoctor.getText().toString().trim();

                if(TextUtils.isEmpty(diagnostico)){
                    txvdiagnosticodoctor.setError("El campo no puede quedar vacio");
                    txvdiagnosticodoctor.requestFocus();
                }else if(TextUtils.isEmpty(tratamiento)){
                    txvtratamientodoctor.setError("El campo no puede quedar vacio");
                    txvtratamientodoctor.requestFocus();
                }else{

                   consulta.setIdConsulta(IDconsulta);
                   consulta.setDia(tvfechaHistorial2.getText().toString().trim());
                   consulta.setHora(tvhoraHistorial2.getText().toString().trim());
                   consulta.setDiagnostico(txvdiagnosticodoctor.getText().toString().trim());
                   consulta.setTratamiento(txvtratamientodoctor.getText().toString().trim());
                    mostrarDialogoBasico(consulta);


                }


            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);
                finish();
                //System.exit(0);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void mostrarDialogoBasico(final Consulta consulta){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(ExpedienteDoctor.this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("¿Desea terminar la consulta?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                    aceptar(consulta);

                    ActualizarEstado(consulta);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialogo1.show();
    }

    public void aceptar(final Consulta consulta) {

        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/consulta/create.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("dia", consulta.getDia());
            jsonObject.put("hora", consulta.getHora());
            jsonObject.put("estado", "Atendido");
            jsonObject.put("diagnostico", consulta.getDiagnostico());
            jsonObject.put("tratamiento",consulta.getTratamiento());
            jsonObject.put("id_cita", consulta.getIdConsulta());
            txvdiagnosticodoctor.setText("");
            txvtratamientodoctor.setText("");

        }catch (JSONException e){
            System.out.println("*********ERROR GRAVE1************");
            e.printStackTrace();
            System.out.println("**********************");
            System.out.println("ERROR AL INSETAR LOS DATOS: " +e.getMessage());
            Toast.makeText(getApplicationContext(), "Hubo un error inesperedo al insertar los datos", Toast.LENGTH_LONG).show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Datos Correctamente Insertados", Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor Al Insertar" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);


    }

    public void cancelar() {
        Toast t=Toast.makeText(getApplicationContext(),"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }


    public void ActualizarEstado(final Consulta consulta){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/cita/CambiarEstado.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("id_cita", consulta.getIdConsulta());
            jsonObject.put("estado", "Atendido");

        }catch (JSONException e){
            System.out.println("*********ERROR GRAVE2************");
            e.printStackTrace();
            System.out.println("**********************");
            System.out.println("ERROR AL actualizarrr LOS DATOS: " +e.getMessage());
            //Toast.makeText(getApplicationContext(), "Hubo un error  al Actualizar los datos", Toast.LENGTH_LONG).show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(getApplicationContext(), "Datos Correctamente Actualizado", Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor Al Actualizar" +error.toString());
                //Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                 //       " "+error, Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);
    }


}
