package com.example.clinicautec.Controlador.Paciente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.clinicautec.MainActivity;
import com.example.clinicautec.R;

public class MenuPaciente extends AppCompatActivity {

    CardView misconsultas,solicitudes;
    SharedPreferences session;

    String id2,nombre2,apellido2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_paciente);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        session = getSharedPreferences("session", Context.MODE_PRIVATE);
        String id = session.getString("id_usuario", "");
        String nombre = session.getString("nombre", "");
        String apellido = session.getString("apellido", "");
        int rol = session.getInt("rol", 0);

        //Toast.makeText(getApplicationContext(), "Nombre " +nombre +" ID: " +id, Toast.LENGTH_LONG).show();

        misconsultas = findViewById(R.id.carvimisconsultas);
        solicitudes = findViewById(R.id.carvisolicitud);

        id2=id;
        misconsultas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HistorialConsultaPaciente.class);
                i.putExtra("idExpediente", id2);
                Toast.makeText(getApplicationContext(), "Expediente" +id2, Toast.LENGTH_LONG).show();
                startActivity(i);
            }
        });

        solicitudes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), HistorialSolicitudesPaciente.class);
                i.putExtra("idExpediente", id2);
                Toast.makeText(getApplicationContext(), "Expediente" +id2, Toast.LENGTH_LONG).show();
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);
                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
