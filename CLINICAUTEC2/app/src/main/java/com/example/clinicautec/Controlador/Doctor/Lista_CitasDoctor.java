package com.example.clinicautec.Controlador.Doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.DAO.ExpedienteDao;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.R;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Lista_CitasDoctor extends AppCompatActivity {

    ListView lv;
    ArrayList<Cita> lista;
    RecordListPaciente adaptador = null;
    Button btBuscarUsuc,btAtrasUsu;
    //ExpedienteDao expedienteDa;

    //Lista para usar en los componentes de Android
    ArrayList<String> lista2;

    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista__citas_doctor);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        lv =(ListView)findViewById(R.id.lvListaCitas);

        //expedienteDa = new ExpedienteDao(getApplicationContext());
        //lista = expedienteDa.getExpediente2();
        String User = getIntent().getStringExtra("RolUser");
        listaExpedienteWS(User);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

    }

    public void listaExpedienteWS(String user){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/personal/listaHistorialDoct.php?id="+user;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(getApplicationContext(), "Se Selecciona 2", Toast.LENGTH_LONG).show();
                String dato3 = getIntent().getStringExtra("dato");
                lista2 = new ArrayList<String>();
                lista = new ArrayList<>();
                try {
                    JSONArray json=response.getJSONArray("data");
                    for (int i=0;i<json.length();i++){
                        Cita c = new Cita();
                        Expediente e = new Expediente();
                        Paciente p =new Paciente();
                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(i);
                        c.setIdCita(jsonObject.optInt("id_cita"));
                        c.setEstado(jsonObject.optString("estado"));
                        e.setIdExpediente(jsonObject.optString("id_expediente"));
                        p.setNombre(jsonObject.optString("paciente_nombre"));
                        p.setApellido(jsonObject.optString("paciente_apellido"));
                        p.setTelefono(jsonObject.optString("paciente_telefono"));
                        p.setCorreo(jsonObject.optString("paciente_correo"));
                        e.setPaciente(p);
                        c.setExpediente(e);
                        lista.add(c);
                        //LLenar  el jsonObject para llenar la lista
                        lista2.add(jsonObject.getString("id_cita") +" " +jsonObject.getString("estado") + " " +jsonObject.getString("id_expediente"));

                        // System.out.println("++++++++++++ESTE ES EL VAlor");
                        //System.out.println(lista);
                        adaptador = new RecordListPaciente(Lista_CitasDoctor.this , R.layout.activity_row_paciente__doctor, lista,dato3);
                        lv.setAdapter(adaptador);
                        adaptador.notifyDataSetChanged();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No Hay ninguna Cita programada" , Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
