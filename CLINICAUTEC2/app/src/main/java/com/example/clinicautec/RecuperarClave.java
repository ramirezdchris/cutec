package com.example.clinicautec;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class RecuperarClave extends AppCompatActivity {

    Button btnCorreoRecuperar;
    EditText edtCorreoRecuperar;

    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_clave);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        btnCorreoRecuperar = findViewById(R.id.btnCorreoRecuperar);
        edtCorreoRecuperar = findViewById(R.id.edtCorreoRecuperar);

        btnCorreoRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo = edtCorreoRecuperar.getText().toString().trim();
                if(TextUtils.isEmpty(correo)){
                    edtCorreoRecuperar.setError("Digite el correo");
                    edtCorreoRecuperar.requestFocus();
                }else {
                    boolean esCorreo = Patterns.EMAIL_ADDRESS.matcher(correo).matches();
                    if (!esCorreo) {
                        edtCorreoRecuperar.setError("Digite un correo valido");
                        edtCorreoRecuperar.requestFocus();
                    }else{
                        enviarCorreo(correo);
                    }
                }
            }
        });
    }

    private void enviarCorreo(String correo){
        String url = "http://192.168.1.15:8080/webservice/WebService/PHPMailer/enviarCodigo.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("user", correo);


        }catch (JSONException e){
            System.out.println("ERRRRORRRR EN EL CATCH enviarCorreo: " +e.getMessage());
            Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getApplicationContext(), response.getString("mensaje"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);
    }
}
