package com.example.clinicautec.Modelo;

public class Cita {

    private int idCita;
    private String dia;
    private String hora;
    private String estado;
    private Personal personal;
    private Expediente expediente;

    public Cita() {
    }

    public Cita(int idCita, String dia, String hora,String estado, Personal personal, Expediente expediente) {
        this.idCita = idCita;
        this.dia = dia;
        this.hora = hora;
        this.estado = estado;
        this.personal = personal;
        this.expediente = expediente;
    }

    public Cita(String dia, String hora,String estado, Personal personal, Expediente expediente) {
        this.dia = dia;
        this.hora = hora;
        this.estado = estado;
        this.personal = personal;
        this.expediente = expediente;
    }

    public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Personal getPersonal() {
        return personal;
    }

    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    @Override
    public String toString() {
        return "Cita{" +
                "idCita=" + idCita +
                ", dia='" + dia + '\'' +
                ", hora='" + hora + '\'' +
                ", estado='" + estado + '\'' +
                ", personal=" + personal +
                ", expediente=" + expediente +
                '}';
    }
}
