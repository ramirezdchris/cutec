package com.example.clinicautec.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.Modelo.Rol;
import com.example.clinicautec.Modelo.Usuario;
import com.example.clinicautec.Util.ConexionSQLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UsuarioDao {
    ConexionSQLiteHelper conn;
    SQLiteDatabase db;

    private RequestQueue queue;

    private Usuario usuario = new Usuario();
    private Personal personal = new Personal();
    private Rol rol = new Rol();

    public UsuarioDao(Context context){
        conn = new ConexionSQLiteHelper(context, "db", null, 1);
        queue = Volley.newRequestQueue(context);
        /*usuario = new Usuario();
        rol = new Rol();
        personal = new Personal();*/
    }

    public void openDB(){
        db = conn.getWritableDatabase(); // Abrimos la base de datos para editarla
    }

    public void closeDB(){
        conn.close();
        db.close();
    }

    public boolean insertar(Usuario usuario){
        try {
            ContentValues values = new ContentValues();
            values.put("usuario" , usuario.getUsuario());
            values.put("pass" , usuario.getPass());
            values.put("id_personal", usuario.getPersonal().getIdPersonal());
            values.put("id_rol", usuario.getRol().getIdRol());
            db.insert("usuario", null, values);
            return true;
        }catch (Exception e){
            System.err.println("Error Metodo Insertar UsuarioDao" +e.getMessage());
            return false;
        }
    }

    public boolean modificar(Usuario usuario){
        try {
            ContentValues values = new ContentValues();
            values.put("usuario", usuario.getUsuario());
            values.put("pass", usuario.getPass());
            values.put("id_personal",usuario.getPersonal().getIdPersonal());
            values.put("id_rol",usuario.getRol().getIdRol());
            db.update("usuario", values, "usuario=?", new String[]{usuario.getUsuario() +""});
            return true;
        }catch (Exception e){
            System.err.println("Error Metodo Modificar UsuarioDao" +e.getMessage());
            return false;
        }
    }

    public boolean eliminar(String usuario){
        try {

            db.delete("usuario", "usuario=" +usuario, null);
            return true;
        }catch (Exception e){
            System.err.println("Error Metodo Eliminar UsuarioDao" +e.getMessage());
            return false;
        }
    }

    public ArrayList<Usuario> getPersonal(){
        ArrayList<Usuario> listaUsuario = new ArrayList<>();
        try {
            Cursor c = db.rawQuery("SELECT * FROM usuario" , null);

            while(c.moveToNext()){
                Usuario usuario = new Usuario();
                Personal personal = new Personal();
                Rol rol = new Rol();


                usuario.setUsuario(c.getString(0));
                usuario.setPass(c.getString(1));
                personal.setIdPersonal(c.getInt(2));
                rol.setIdRol(c.getInt(3));
                usuario.setPersonal(personal);
                usuario.setRol(rol);

                listaUsuario.add(usuario);
            }
            return listaUsuario;
        }catch (Exception e){
            System.err.println("Error Metodo getPersonal UsuarioDao" +e.getMessage());
            return null;
        }
    }

    public ArrayList<Personal> getDoctores(){
        ArrayList<Personal> listaDoctores = new ArrayList<>();
        try {
            Cursor c = conn.getData("SELECT p.id_personal, p.nombre, p.apellido FROM personal p" +
                    " INNER JOIN usuario u ON u.id_personal = p.id_personal" +
                    " INNER JOIN rol rol ON rol.id_rol = u.id_rol " +
                    " WHERE rol.id_rol = 3");

            while(c.moveToNext()){
                Usuario usuario = new Usuario();
                Personal personal = new Personal();
                Rol rol = new Rol();

                personal.setIdPersonal(c.getInt(0));
                personal.setNombre(c.getString(1));
                personal.setApellido(c.getString(2));

                listaDoctores.add(personal);
            }
            return listaDoctores;
        }catch (Exception e){
            System.err.println("Error Metodo getDoctores UsuarioDao" +e.getMessage());
            return null;
        }
    }


    public int loginUsuarios(String user, String pass){
        openDB();
        Cursor c;
        Cursor v;
        try {
            String select[] = new String[]{"id_rol, usuario"};
            String where[] = new String[]{user,pass};
            c = db.query("usuario", select, "usuario = ? AND pass = ?", where, null,null,null);
            c.moveToFirst();
            System.out.println(c.getCount());
            if(c.getCount() == 0){
                return -1;
            }else{
                return c.getInt(0);
            }




        }catch (Exception e){
            System.out.println("Error en login3: " + e.getMessage());
            return -1;
        }
    }

    public int loginPacientes(String correo, String pass){
        openDB();
        Cursor c;
        Cursor v;

        try {
            String select2[] = new String[]{"correo"};
            String where2[] = new String[]{correo,pass};
            v = db.query("paciente", select2, "correo = ? AND pass = ?", where2, null,null,null);

            v.moveToFirst();
            System.out.println(v.getCount());
            System.out.println(v.getString(0));
            if(v.getCount() == 0){
                return -1;
            }else{
                return 4;
            }



        }catch (Exception e){
            System.out.println("Error en login4: " + e.getMessage());
            return -1;
        }
    }



    public Usuario obtenerLoginUsuario(String user, String pass){
        //String url = "http://192.168.1.15:8080/webservice/WebService/API/usuario/login.php?user="+user +"&pass="+pass;

        //URL DE ALEX
        String url = "http://192.168.1.11:8080/webservice/WebService/API/usuario/login.php?user="+user +"&pass="+pass;
        //final int[] rol2;


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //ArrayList usuario = new ArrayList();



                try {
                    /*JSONArray mJSONArray = response.getJSONArray("");

                    //JSONObject mJSONObject = response.getJSONObject("nombre");
                    usuario.add(mJSONObject.getString("nombre"));
                    */
                    System.out.println("DatOOOOOOOOOOOOOOOOOOo: " +response.getString("nombre"));

                    if(response.isNull("rol")){
                        rol.setIdRol(-1);
                        usuario.setRol(rol);
                        //rol2[0] = rol;
                    }else{
                        rol.setIdRol(response.getInt("rol"));
                        usuario.setRol(rol);
                    }

                    System.out.println("ROLLLLLLLLLL: " +rol.getIdRol());
                    System.out.println("ROLLLLLLLLLL222222: " +usuario.getRol().getIdRol());
                    /*
                    for (int i = 0; i < mJSONArray.length(); i++){
                        JSONObject mJSONObject = mJSONArray.getJSONObject(i);
                        usuario.add(mJSONObject.getString("nombre"));
                        usuario.add(mJSONObject.getString("apellido"));
                        usuario.add(mJSONObject.getString("rol"));
                        System.out.println("DatOOOOOOOOOOOOOOOOOOo: " +mJSONObject.getString("nombre"));
                    }

                     */
                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("ErrorRRRRRRRRRRRRRRRRRRR: " +e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                rol = new Rol();
                personal = new Personal();
                usuario = new Usuario();
                System.out.println("onErrorResponseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee: " +error.getMessage());
            }
        });


        if(request.getTimeoutMs() >= 2500){
            request.cancel();
        }else{
            queue.add(request);
            System.out.println("RYYYYYYYYYYYYY " +rol.getIdRol());
            System.out.println("RYYYYYYYY222222: " +usuario.getRol());
        }
        System.out.println("Tiempo: " +request.getTimeoutMs());

        return usuario;
    }

}
