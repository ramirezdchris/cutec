package com.example.clinicautec.Controlador.Doctor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Consulta;
import com.example.clinicautec.R;

import java.util.ArrayList;

public class RecordListHistorial extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Consulta> recordListConsulta;

    public RecordListHistorial(Context context, int layout, ArrayList<Consulta> recordListConsulta){
        this.context = context;
        this.layout = layout;
        this.recordListConsulta = recordListConsulta;
    }

    @Override
    public int getCount() { return recordListConsulta.size(); }

    @Override
    public Object getItem(int i) { return recordListConsulta.get(i); }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder2{
        TextView tvfechadoctor, tvhoradoctor, tvnombredoctor, tvexpedientedoctor;
        Button btnacciondoctorlista;
    }
    @Override
    public View getView(int i, View view, ViewGroup ViewHolder) {
        View row = view;
        ViewHolder2 holder2 = new ViewHolder2();

        if(row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder2.tvfechadoctor = row.findViewById(R.id.tvfechadoctor);
            holder2.tvhoradoctor = row.findViewById(R.id.tvhoradoctor);
            holder2.tvnombredoctor = row.findViewById(R.id.tvnombredoctor);
            holder2.tvexpedientedoctor = row.findViewById(R.id.tvexpedientedoctor);
            holder2.btnacciondoctorlista = row.findViewById(R.id.btnacciondoctorlista);
            row.setTag(holder2);
        }
        else {
            holder2 = (ViewHolder2) row.getTag();
        }

         Consulta consulta= recordListConsulta.get(i);
        final String idConsul = String.valueOf(consulta.getIdConsulta());
        final String Expediente = String.valueOf(consulta.getCita().getExpediente().getIdExpediente());

        holder2.tvfechadoctor.setText(consulta.getDia());
        holder2.tvhoradoctor.setText(consulta.getHora());
        holder2.tvnombredoctor.setText(String.valueOf(consulta.getCita().getPersonal().getNombre()));
        holder2.tvexpedientedoctor.setText(String.valueOf(consulta.getCita().getExpediente().getIdExpediente()));
        holder2.btnacciondoctorlista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogoBasico(idConsul,Expediente);
            }
        });
        return row;
    }

    private void mostrarDialogoBasico(final String idconsulta,final String Expe){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this.context);
        dialogo1.setTitle("VER EXPEDIENTE DE CONSULTA");
        dialogo1.setMessage("¿Desea ver este expediente  ");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar(idconsulta,Expe);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialogo1.create().show();
    }

    public void aceptar(String IDconsul,String expe2) {

        String IDD = String.valueOf(IDconsul);
        String EXX = String.valueOf(expe2);
        Intent objExpediente = new Intent(context, VerExpedienteDoctor.class);
        objExpediente.putExtra("expedientee", EXX);
        objExpediente.putExtra("Idconsulta", IDD);
        context.startActivity(objExpediente);
        Toast t=Toast.makeText(context,"Ver Consulta numero "+IDD, Toast.LENGTH_SHORT);
        t.show();
    }

    public void cancelar() {
        Toast t=Toast.makeText(context,"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }
}
