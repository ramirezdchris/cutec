package com.example.clinicautec.Controlador.Administrador;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Enfermera.RecordListPacientes;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.R;

import java.util.ArrayList;

public class RecordListPersonal extends BaseAdapter {

    private Context context;
    private int layout;
    //private ArrayList<Cita> recordListCita;
    private ArrayList<Personal> recordListPersonal;
    private RequestQueue queue;

    public RecordListPersonal(Context context, int layout,  ArrayList<Personal> recordListExpediente){
        this.context = context;
        this.layout = layout;
        this.recordListPersonal = recordListExpediente;
        queue = Volley.newRequestQueue(context);
    }

    @Override
    public int getCount() {
        return recordListPersonal.size();
    }

    @Override
    public Object getItem(int position) {
        return recordListPersonal.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView tvPersonalNombre, tvDuiPersol, tvCorreoPersonal, tvTelefonoPersonal;
        Button btnPersonalActualizar, btnPersonalEliminar;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordListPersonal.ViewHolder holder = new RecordListPersonal.ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.tvPersonalNombre = row.findViewById(R.id.tvNombrePersonal);
            holder.tvDuiPersol = row.findViewById(R.id.tvDuiPersonal);
            holder.tvCorreoPersonal = row.findViewById(R.id.tvCorreoPersonal);
            holder.tvTelefonoPersonal = row.findViewById(R.id.tvTelefonoPersonal);
            holder.btnPersonalActualizar = row.findViewById(R.id.btnPersonalActualizar);
            holder.btnPersonalEliminar = row.findViewById(R.id.btnPersonalEliminar);
            row.setTag(holder);
        }else{
            holder = (RecordListPersonal.ViewHolder)row.getTag();
        }

        final Personal personal = recordListPersonal.get(position);


        holder.tvPersonalNombre.setText(personal.getNombre() + " " +personal.getApellido());
        holder.tvDuiPersol.setText(personal.getDui());
        holder.tvCorreoPersonal.setText(personal.getCorreo());
        holder.tvTelefonoPersonal.setText(personal.getTelefono());
        holder.btnPersonalActualizar = row.findViewById(R.id.btnPersonalActualizar);
        holder.btnPersonalEliminar = row.findViewById(R.id.btnPersonalEliminar);


        final int id_personal = personal.getIdPersonal();
        final String nombre = personal.getNombre();
        final String apellido = personal.getApellido();
        final String dui = personal.getDui();
        final String telefono = personal.getTelefono();
        final String correo = personal.getCorreo();


        holder.btnPersonalActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objP = new Intent(context, AgregarPersonalAdministrador.class);
                objP.putExtra("opcion" , 1);
                objP.putExtra("id_personal" , id_personal);
                objP.putExtra("nombreP", nombre);
                objP.putExtra("apellidoP", apellido);
                objP.putExtra("duiP", dui);
                objP.putExtra("telefonoP", telefono);
                objP.putExtra("correoP", correo);
                context.startActivity(objP);
            }
        });

        holder.btnPersonalEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mostrarDialogEliminar(expediente);
            }
        });

        return row;
    }
    /*
    private mostrarDialogActualizar(){
        AlertDialog.Builder dialogo2 = new AlertDialog.Builder(this.context);
        dialogo2.setTitle("Actualizar Paciente");
        dialogo2.setMessage("¿Desea actualizar al Paciente  "+nombre +" " +apellido  +"?");
        dialogo2.setCancelable(false);
        dialogo2.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo2, int id) {
                aceptar1(id_expediente, nombre, apellido, dui, carnet, telefono, fecha_nacimiento, correo, pass, id_paciente);
            }
        });
        dialogo2.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo2, int id) {
                dialogo2.dismiss();
            }
        });
        dialogo2.create().show();
    }*/
}
