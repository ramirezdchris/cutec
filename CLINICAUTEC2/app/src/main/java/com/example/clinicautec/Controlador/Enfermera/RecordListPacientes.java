package com.example.clinicautec.Controlador.Enfermera;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Doctor.ExpedienteDoctor;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class RecordListPacientes extends BaseAdapter {

    private Context context;
    private int layout;
    //private ArrayList<Cita> recordListCita;
    private ArrayList<Expediente> recordListExpediente;
    private RequestQueue queue;

    public RecordListPacientes(Context context, int layout,  ArrayList<Expediente> recordListExpediente){
        this.context = context;
        this.layout = layout;
        this.recordListExpediente = recordListExpediente;
        queue = Volley.newRequestQueue(context);
    }

    @Override
    public int getCount() {
        return recordListExpediente.size();
    }

    @Override
    public Object getItem(int position) {
        return recordListExpediente.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView tvIdExpediente, tvPacienteNombreCompleto, tvPacienteDUI, tvPacienteCarnet;
        Button btnEnfeActualizar, btnEnfeEliminar;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordListPacientes.ViewHolder holder = new RecordListPacientes.ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.tvIdExpediente = row.findViewById(R.id.tvExpedientePaciente);
            holder.tvPacienteNombreCompleto = row.findViewById(R.id.tvPacienteNombreCompleto);
            holder.tvPacienteDUI = row.findViewById(R.id.tvPacienteDUI);
            holder.tvPacienteCarnet = row.findViewById(R.id.tvPacienteCarnet);
            holder.btnEnfeActualizar = row.findViewById(R.id.btnEnfeActualizar);
            holder.btnEnfeEliminar = row.findViewById(R.id.btnEnfeEliminar);
            row.setTag(holder);
        }else{
            holder = (RecordListPacientes.ViewHolder)row.getTag();
        }

        final Expediente expediente = recordListExpediente.get(position);

        holder.tvIdExpediente.setText(expediente.getIdExpediente());
        holder.tvPacienteNombreCompleto.setText(expediente.getPaciente().getNombre() + " " +expediente.getPaciente().getApellido());
        holder.tvPacienteDUI.setText(expediente.getPaciente().getDui());
        holder.tvPacienteCarnet.setText(expediente.getPaciente().getCarnet());
        //holder.btnEnfeActualizar = row.findViewById(R.id.btnEnfeActualizar);
        //holder.btnEnfeEliminar = row.findViewById(R.id.btnEnfeEliminar);


        final String id_expediente = expediente.getIdExpediente();
        final String nombre = expediente.getPaciente().getNombre();
        final String apellido = expediente.getPaciente().getApellido();
        final String dui = expediente.getPaciente().getDui();
        final String carnet = expediente.getPaciente().getCarnet();
        final String telefono = expediente.getPaciente().getTelefono();
        final String fecha_nacimiento = expediente.getPaciente().getFechaNacimiento();
        final String correo = expediente.getPaciente().getCorreo();
        final String pass = expediente.getPaciente().getPass();
        final int id_paciente = expediente.getPaciente().getIdPaciente();

        holder.btnEnfeActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogoBasico(id_expediente, nombre, apellido, dui, carnet, telefono, fecha_nacimiento, correo, pass, id_paciente);
                //Toast.makeText(context, "Hola", Toast.LENGTH_SHORT).show();
            }
        });

        holder.btnEnfeEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogEliminar(expediente);
            }
        });

        return row;
    }


    private void mostrarDialogoBasico(final String id_expediente,final String nombre,final String apellido,final String dui,final String carnet,final String telefono,final String fecha_nacimiento,final String correo,final String pass, final int id_paciente){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        AlertDialog.Builder dialogo2 = new AlertDialog.Builder(this.context);
        dialogo2.setTitle("Actualizar Paciente");
        dialogo2.setMessage("¿Desea actualizar al Paciente  "+nombre +" " +apellido  +"?");
        dialogo2.setCancelable(false);
        dialogo2.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo2, int id) {
                aceptar1(id_expediente, nombre, apellido, dui, carnet, telefono, fecha_nacimiento, correo, pass, id_paciente);
            }
        });
        dialogo2.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo2, int id) {
                dialogo2.dismiss();
            }
        });
        dialogo2.create().show();
    }

    public void aceptar1(final String id_expediente,final String nombre,final String apellido,final String dui,final String carnet,final String telefono,final String fecha_nacimiento,final String correo,final String pass, final int id_paciente) {
        //String exp = String.valueOf(expediente);
        //String idcita = String.valueOf(Idcita);
        Intent objAgregarPaciente = new Intent(context, AgregarPacientes.class);
        objAgregarPaciente.putExtra("expediente" , id_expediente);
        objAgregarPaciente.putExtra("nombre" , nombre);
        objAgregarPaciente.putExtra("apellido" , apellido);
        objAgregarPaciente.putExtra("dui" , dui);
        objAgregarPaciente.putExtra("carnet" , carnet);
        objAgregarPaciente.putExtra("telefono" , telefono);
        objAgregarPaciente.putExtra("fecha_nacimiento" , fecha_nacimiento);
        objAgregarPaciente.putExtra("correo" , correo);
        objAgregarPaciente.putExtra("pass" , pass);
        objAgregarPaciente.putExtra("id_paciente", id_paciente);
        objAgregarPaciente.putExtra("actualizar", "actualizar");
        context.startActivity(objAgregarPaciente);

    }

    private void mostrarDialogEliminar(final Expediente expediente){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        AlertDialog.Builder dialogo = new AlertDialog.Builder(this.context);
        dialogo.setTitle("Eliminar Paciente");
        dialogo.setMessage("¿Esta seguro que sea eliminar al paciente "+expediente.getPaciente().getNombre() +" " +expediente.getPaciente().getApellido() + "?");
        dialogo.setCancelable(false);
        dialogo.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo, int id) {
                eliminarPaciente(expediente);
            }
        });
        dialogo.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo, int id) {
                dialogo.dismiss();
            }
        });



        dialogo.create().show();
    }

    private void eliminarPaciente(final Expediente expediente){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/paciente/delete.php?id_paciente="+expediente.getPaciente().getIdPaciente();

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        System.out.println("AQUI VIENE EL ID: " +expediente.getPaciente().getIdPaciente());


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(context, "Se elimino correctamente el paciente", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);
    }
}
