package com.example.clinicautec.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ArrayAdapter;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Util.ConexionSQLiteHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ExpedienteDao {

    ConexionSQLiteHelper conn;
    SQLiteDatabase db;

    ArrayList lista3;
    ArrayList<Expediente> listaExpe;
    Expediente expediente = new Expediente();
    Paciente paciente = new Paciente();

    RequestQueue queue;

    public ExpedienteDao(Context context){
        conn = new ConexionSQLiteHelper(context, "db", null, 1);
        queue = Volley.newRequestQueue(context);;
    }

    public void openDB(){
        db = conn.getWritableDatabase(); // Abrimos la base de datos para editarla
    }

    public void closeDB(){
        conn.close();
        db.close();
    }


    public ArrayList<Expediente> getPacientesExpediente(){
        ArrayList<Expediente> listaExpedientes = new ArrayList<>();
        try {
            Cursor c = conn.getData("SELECT * FROM expediente ex" +
                    " INNER JOIN paciente paci ON paci.id_paciente = ex.id_paciente;");

            while(c.moveToNext()){
                Expediente expediente = new Expediente();

                Paciente paciente = new Paciente();
                expediente.setIdExpediente(c.getString(0));
                expediente.setFechaCreacion(c.getString(1));

                paciente.setIdPaciente(c.getInt(3));
                paciente.setNombre(c.getString(4));
                paciente.setApellido(c.getString(5));
                paciente.setDui(c.getString(6));
                paciente.setTelefono(c.getString(7));
                paciente.setFechaNacimiento(c.getString(8));

                expediente.setPaciente(paciente);
                listaExpedientes.add(expediente);
            }
            return listaExpedientes;
        }catch (Exception e){
            System.err.println("Error Metodo getPacientesExpediente ExpedienteDao" +e.getMessage());
            return null;
        }
    }

    public ArrayList<Expediente> getExpediente2(){
        ArrayList<Expediente> listaexpediente = new ArrayList<>();
        try {
            Cursor cursor = conn.getData("SELECT e.id_expediente,p.nombre,p.telefono,p.apellido,p.correo " +
                    "FROM paciente as p Inner Join expediente as e on e.id_paciente = p.id_paciente");

            while(cursor.moveToNext()){
                String id = cursor.getString(0);
                String nombre = cursor.getString(1);
                String telefono = cursor.getString(2);
                String apellido = cursor.getString(3);
                String email = cursor.getString(4);
                Paciente p =new Paciente();
                p.setNombre(nombre);
                p.setTelefono(telefono);
                p.setCorreo(email);
                Expediente e = new Expediente();
                e.setIdExpediente(id);
                e.setPaciente(p);
                listaexpediente.add(e);
            }
            System.err.println("Eso trae " +listaexpediente);
            return listaexpediente;
        }catch (Exception e){
            System.err.println("Error Metodo getExpediente2 ExpedienteDao " +e.getMessage());
            return  null;
        }

    }


    public ArrayList<Expediente> obtenerExpedientes(){

        listaExpe = new ArrayList<>();
        expediente.setIdExpediente("0000");
        listaExpe.add(expediente);
        //String url = "https://api.androidhive.info/contacts/";
        String url = "http://192.168.1.15:8080/webservice/WebService/API/expediente/read_expedientes.php";


                JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println("Aqui estamos 1 ");
                        ArrayList listaP = new ArrayList();
                        lista3 = new ArrayList<String>();

                        listaExpe = new ArrayList<Expediente>();

                        try {

                            JSONArray mJSONArray = response.getJSONArray("data");
                            for(int i=0; i < mJSONArray.length(); i++){
                                JSONObject mJSONObject = mJSONArray.getJSONObject(i);

                                expediente = new Expediente();
                                paciente = new Paciente();

                                expediente.setIdExpediente(mJSONObject.getString("id_expediente"));
                                paciente.setNombre(mJSONObject.getString("nombre"));
                                paciente.setApellido(mJSONObject.getString("apellido"));
                                expediente.setPaciente(paciente);

                                // Para obtener las variables
                                //String name = mJSONObject.getString("nombre");
                                //lista3.add(expediente);
                                listaExpe.add(expediente);
                                lista3.add(mJSONObject.getString("id_expediente") +" " +mJSONObject.getString("nombre") + " " +mJSONObject.getString("apellido"));

                            }

                        }catch (JSONException e){
                            e.printStackTrace();
                            System.out.println("Error: " +e.getMessage());
                            //Toast.makeText(ListadoPacientes.this, "Error: " +e.getMessage() , Toast.LENGTH_SHORT).show();
                        }

                        //adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, lista3);

                        System.out.println("LISTA \n" +lista3);
                        //System.out.println("LISTA2 \n\n" +expedienteDao.getPacientesExpediente());
                        //listaExpedientes.setAdapter(adapter);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Aqui estamos 2 " +error.getMessage());
            }
        });

        queue.add(request);
        return listaExpe;
    }
}
