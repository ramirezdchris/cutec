package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.R;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Matcher;

public class AgregarPacientes extends AppCompatActivity {

    Button btnAgregarPaciente;
    EditText edtNombrePaciente, edtApellidoPaciente, edtCarnet, edtTelefono, edtFechaNacimiento, edtCorreo;
    EditText edtDui;
    Switch aSwitch;


    Paciente paciente;

    int id_paciente;
    String pass;

    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_pacientes);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        edtNombrePaciente = findViewById(R.id.edtNombrePaciente);
        edtApellidoPaciente = findViewById(R.id.edtApellidoPaciente);
        edtDui = findViewById(R.id.edtDui);
        aSwitch = findViewById(R.id.switch1);
        edtCarnet = findViewById(R.id.edtCarnet);
        edtTelefono = findViewById(R.id.edtTelefono);
        edtFechaNacimiento = findViewById(R.id.edtFechaNacimiento);
        edtCorreo = findViewById(R.id.edtCorreo);
        btnAgregarPaciente = findViewById(R.id.btnAgregarPaciente);


        edtCarnet.setVisibility(View.INVISIBLE);

        // Mask
        SimpleMaskFormatter smfDui = new SimpleMaskFormatter("NNNNNNNN-N");
        MaskTextWatcher mtwDui = new MaskTextWatcher(edtDui, smfDui);
        SimpleMaskFormatter smfCarnet = new SimpleMaskFormatter("NN-NNNN-NNNN");
        MaskTextWatcher mtwCarnet = new MaskTextWatcher(edtCarnet, smfCarnet);
        SimpleMaskFormatter smfTelefono = new SimpleMaskFormatter("NNNN-NNNN");
        MaskTextWatcher mtwTelefono = new MaskTextWatcher(edtTelefono, smfTelefono);

        // Aplicar mascaras
        edtDui.addTextChangedListener(mtwDui);
        edtCarnet.addTextChangedListener(mtwCarnet);
        edtTelefono.addTextChangedListener(mtwTelefono);

        //Objetos
        paciente = new Paciente();

        // Recibiendo si es actualizar

        String opcion = getIntent().getStringExtra("actualizar");
        if(opcion.equals("actualizar")){
            String nombre, apellido, dui, carnet, telefono, fecha_nacimiento, correo;

            id_paciente = getIntent().getIntExtra("id_paciente", 0);
            nombre = getIntent().getStringExtra("nombre");
            apellido = getIntent().getStringExtra("apellido");
            dui = getIntent().getStringExtra("dui");
            carnet = getIntent().getStringExtra("carnet");
            telefono = getIntent().getStringExtra("telefono");
            fecha_nacimiento = getIntent().getStringExtra("fecha_nacimiento");
            correo = getIntent().getStringExtra("correo");
            pass = getIntent().getStringExtra("pass");

            btnAgregarPaciente.setText("Actualizar Paciente");
            edtNombrePaciente.setText(nombre);
            edtApellidoPaciente.setText(apellido);
            edtDui.setText(dui);
            edtCarnet.setText(carnet);
            edtTelefono.setText(telefono);
            edtFechaNacimiento.setText(fecha_nacimiento);
            edtCorreo.setText(correo);
        }else{

        }


        aSwitch.setTextOff("Si");
        aSwitch.setTextOn("No");
        edtCarnet.setText("00-0000-0000");
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(aSwitch.isChecked()){
                    edtCarnet.setVisibility(View.VISIBLE);
                    edtCarnet.setText("");
                }else{
                    edtCarnet.setVisibility(View.INVISIBLE);
                    edtCarnet.setText("No carnet");
                }

            }
        });

        edtFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AgregarPacientes.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        final int mesActual = month + 1;
                        String diaFormateado = (dayOfMonth < 10) ? "0" +String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                        String mesFormateado = (mesActual < 10) ? "0" +String.valueOf(mesActual) : String.valueOf(mesActual);
                        edtFechaNacimiento.setText(year + "-" +mesFormateado +"-" +diaFormateado);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });




        btnAgregarPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Obteniendo datos

                String nombre;
                String apellido;
                String dui;
                String carnet;
                String telefono;
                String fecha_nacimiento;
                String correo;

                nombre = edtNombrePaciente.getText().toString().trim();
                apellido = edtApellidoPaciente.getText().toString().trim();
                dui = edtDui.getText().toString().trim();
                carnet = edtCarnet.getText().toString().trim();
                telefono = edtTelefono.getText().toString().trim();
                fecha_nacimiento = edtFechaNacimiento.getText().toString().trim();
                correo = edtCorreo.getText().toString().trim();

                if(TextUtils.isEmpty(nombre)){
                    edtNombrePaciente.setError("Digite el nombre del paciente");
                    edtNombrePaciente.requestFocus();
                }else if(TextUtils.isEmpty(apellido)){
                    edtApellidoPaciente.setError("Digite el apellido del paciente");
                    edtApellidoPaciente.requestFocus();
                }else if(TextUtils.isEmpty(dui)){
                    edtDui.setError("Digite el DUI del paciente");
                    edtDui.requestFocus();
                }else if(TextUtils.isEmpty(telefono)){
                    edtTelefono.setError("Digite el telefono");
                    edtTelefono.requestFocus();
                }else if(TextUtils.isEmpty(fecha_nacimiento)){
                    edtFechaNacimiento.setError("Seleccion un fecha de nacimiento");
                    edtFechaNacimiento.requestFocus();
                }else if(TextUtils.isEmpty(correo)){
                    edtCorreo.setError("Digite el correo electronico");
                    edtCorreo.requestFocus();
                }else if(TextUtils.isEmpty(carnet)){
                    edtCarnet.setError("Digite el carnet del paciente");
                    edtCarnet.requestFocus();
                }else{
                    boolean esCorreo = Patterns.EMAIL_ADDRESS.matcher(correo).matches();
                    if(!esCorreo){
                        edtCorreo.setError("Digite un correo valido");
                        edtCorreo.requestFocus();
                    }else if(dui.length() < 10){
                        edtDui.setError("El dui tiene que contener 10 digitos");
                        edtDui.requestFocus();
                    }else if(telefono.length() < 9){
                        edtTelefono.setError("Numero telefonico debe contener 8 digitos");
                        edtTelefono.requestFocus();
                    }else if(carnet.length() < 12){
                        edtCarnet.setError("Numero de carnet debe contener 12 caracteres");
                        edtCarnet.requestFocus();
                    }
                    else{
                        paciente.setNombre(nombre);
                        paciente.setApellido(apellido);
                        paciente.setDui(dui);
                        paciente.setCarnet(carnet);
                        paciente.setTelefono(telefono);
                        paciente.setFechaNacimiento(fecha_nacimiento);
                        paciente.setCorreo(correo);
                        paciente.setPass(dui);
                        Date date = new Date();
                        System.out.println("Dia: " +date.getDay() +" Mes: " +date.getMonth() +" Año: " +date.getYear());

                        String opcion = btnAgregarPaciente.getText().toString().trim();

                        if(opcion.equals("Actualizar Paciente")){
                            paciente.setIdPaciente(id_paciente);
                            paciente.setPass(pass);
                            mostrarDialog2(paciente);
                        }else{
                            mostrarDialog(paciente);
                        }


                    }

                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    public void mostrarDialog(final Paciente paciente){
        AlertDialog.Builder builder = new AlertDialog.Builder(AgregarPacientes.this);
        //AlertDialog.Builder builder = new AlertDialog.Builder(RecordListPacientes.this.context);
        //LayoutInflater la = getLayoutInflater();
        builder.setTitle("Agregar Paciente");
        builder.setMessage("¿Quieres agregar a " +paciente.getNombre() +" " +paciente.getApellido() +"?").setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //citasD = new CitasDao(getApplicationContext());
                String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/paciente/create.php";

                JSONObject jsonObject = null;
                Map<String,String> params = new Hashtable<String,String>();

                try {
                    jsonObject = new JSONObject();
                    jsonObject.put("nombre", paciente.getNombre());
                    jsonObject.put("apellido" , paciente.getApellido());
                    jsonObject.put("dui" , paciente.getDui());
                    jsonObject.put("carnet" , paciente.getCarnet());
                    jsonObject.put("correo" , paciente.getCorreo());
                    jsonObject.put("telefono" , paciente.getTelefono());
                    jsonObject.put("fecha_nacimiento" , paciente.getFechaNacimiento());
                    jsonObject.put("pass" , paciente.getPass());


                }catch (JSONException e){
                    System.out.println("ERRRRORRRR EN EL CATCH Agregar Paciente: " +e.getMessage());
                    Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), "Se inserto correctamente", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }){

                };

                queue.add(request);

            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "No se realizo la cita ", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).show();
    }

    private void mostrarDialog2(final Paciente paciente){
        AlertDialog.Builder builder = new AlertDialog.Builder(AgregarPacientes.this);
        //AlertDialog.Builder builder = new AlertDialog.Builder(RecordListPacientes.this.context);
        //LayoutInflater la = getLayoutInflater();
        builder.setTitle("Actualizar Paciente");
        builder.setMessage("¿Quieres actualizar los datos a " +paciente.getNombre() +" " +paciente.getApellido() +"?").setPositiveButton("Actualizar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //citasD = new CitasDao(getApplicationContext());
                String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/paciente/update.php";

                JSONObject jsonObject = null;
                Map<String,String> params = new Hashtable<String,String>();

                try {
                    jsonObject = new JSONObject();
                    jsonObject.put("nombre", paciente.getNombre());
                    jsonObject.put("apellido" , paciente.getApellido());
                    jsonObject.put("dui" , paciente.getDui());
                    jsonObject.put("carnet" , paciente.getCarnet());
                    jsonObject.put("correo" , paciente.getCorreo());
                    jsonObject.put("telefono" , paciente.getTelefono());
                    jsonObject.put("fecha_nacimiento" , paciente.getFechaNacimiento());
                    jsonObject.put("pass" , paciente.getPass());
                    jsonObject.put("id_paciente", paciente.getIdPaciente());

                }catch (JSONException e){
                    System.out.println("ERRRRORRRR EN EL CATCH Agregar Paciente: " +e.getMessage());
                    Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), "Se actualizo correctamente", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }){

                };

                queue.add(request);

            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "No se realizo la cita ", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).show();
    }
}
