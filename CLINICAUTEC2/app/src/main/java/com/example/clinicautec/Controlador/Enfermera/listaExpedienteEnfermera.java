package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class listaExpedienteEnfermera extends AppCompatActivity {

    FloatingActionButton fabAgregarEnfermera;
    ListView lv;
    Button btnbuscarEE;
    EditText edtbuscarEE;

    ArrayList<Expediente> lista;
    ArrayList<String> lista2;
    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;

    RecordListaExpedientes adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_expediente_enfermera);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        lv = findViewById(R.id.lvSolicitudEnfermera);
        edtbuscarEE = findViewById(R.id.edtbuscarEE);
        btnbuscarEE = findViewById(R.id.btnbuscarEE);

        btnbuscarEE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String carnet = edtbuscarEE.getText().toString().trim();

                if(TextUtils.isEmpty(carnet)){
                    edtbuscarEE.setError("Digite un expediente");
                    edtbuscarEE.requestFocus();
                }else{
                    BuscarExpediente(carnet);
                }
            }
        });

        ListaSolicitudes();
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void ListaSolicitudes(){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/expediente/read.php";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                lista2 = new ArrayList<String>();
                lista = new ArrayList<>();
                try {
                    JSONArray json=response.getJSONArray("data");
                    for (int i=0;i<json.length();i++){
                        Expediente e = new Expediente();
                        Paciente p = new Paciente();

                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(i);
                        e.setIdExpediente(jsonObject.optString("id_expediente"));
                        e.setFechaCreacion(jsonObject.optString("fecha_creacion"));
                        p.setIdPaciente(jsonObject.optInt("id_paciente"));
                        p.setNombre(jsonObject.optString("nombre"));
                        p.setApellido(jsonObject.optString("apellido"));
                        p.setDui(jsonObject.optString("dui"));
                        p.setCarnet(jsonObject.optString("carnet"));
                        e.setPaciente(p);

                        lista.add(e);
                        //LLenar  el jsonObject para llenar la lista
                        lista2.add(jsonObject.getString("id_expediente") +" " +jsonObject.getString("fecha_creacion") + " " +jsonObject.getString("id_paciente"));

                        adaptador = new RecordListaExpedientes(listaExpedienteEnfermera.this, R.layout.activity_row_expedientes_m_enfermera, lista);
                        lv.setAdapter(adaptador);
                        adaptador.notifyDataSetChanged();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Hubo un Error al listar la lista" +
                            " "+response, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }

    public void BuscarExpediente(String Bucar){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/expediente/BuscarIDexpediente.php?id="+Bucar;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                lista2 = new ArrayList<String>();
                lista = new ArrayList<>();
                try {
                    JSONArray json=response.getJSONArray("data");
                    for (int i=0;i<json.length();i++){
                        Expediente e = new Expediente();
                        Paciente p = new Paciente();

                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(i);
                        e.setIdExpediente(jsonObject.optString("id_expediente"));
                        e.setFechaCreacion(jsonObject.optString("fecha_creacion"));
                        p.setIdPaciente(jsonObject.optInt("id_paciente"));
                        p.setNombre(jsonObject.optString("nombre"));
                        p.setApellido(jsonObject.optString("apellido"));
                        p.setDui(jsonObject.optString("dui"));
                        p.setCarnet(jsonObject.optString("carnet"));
                        e.setPaciente(p);

                        lista.add(e);
                        //LLenar  el jsonObject para llenar la lista
                        lista2.add(jsonObject.getString("id_expediente") +" " +jsonObject.getString("fecha_creacion") + " " +jsonObject.getString("id_paciente"));

                        adaptador = new RecordListaExpedientes(listaExpedienteEnfermera.this, R.layout.activity_row_expedientes_m_enfermera, lista);
                        lv.setAdapter(adaptador);
                        adaptador.notifyDataSetChanged();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Hubo un Error al listar la lista" +
                            " "+response, Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }

}
