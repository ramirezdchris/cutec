package com.example.clinicautec.Util;

public class Constantes {

    public static final String CREAR_TABLA_PERSONAL = "CREATE TABLE personal (" +
            "id_personal INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nombre TEXT," +
            "apellido TEXT," +
            "dui TEXT," +
            "correo TEXT," +
            "telefono TEXT" +
            ");";


    public static final String CREAR_TABLA_ROL = "CREATE TABLE rol (" +
            " id_rol INTEGER PRIMARY KEY AUTOINCREMENT," +
            " rol TEXT " +
            ");";



    public static final String CREAR_TABLA_USUARIO = "CREATE TABLE usuario (" +
            " usuario TEXT," +
            " pass TEXT," +
            " id_personal INTEGER," +
            " id_rol INTEGER," +
            " PRIMARY KEY(usuario)," +
            " FOREIGN KEY(id_personal) REFERENCES personal(id_personal)," +
            " FOREIGN KEY(id_rol) REFERENCES rol(id_rol)" +
            ");";


    public static final String CREAR_TABLA_PACIENTE = "CREATE TABLE paciente(" +
            "id_paciente INTEGER PRIMARY KEY AUTOINCREMENT," +
            "nombre TEXT," +
            "apellido TEXT," +
            "dui TEXT," +
            // por si se ocupa pero trambien agregar a los Models"carnet TEXT" +
            "telefono TEXT," +
            "fecha_nacimiento TEXT, " +
            "correo TEXT," +
            "pass TEXT" +
            ");";



    public static final String CREAR_TABLA_EXPEDIENTE = "CREATE TABLE expediente(" +
            "id_expediente TEXT," +
            "fecha_creacion TEXT," +
            "id_paciente INTEGER," +
            "PRIMARY KEY(id_expediente)," +
            "FOREIGN KEY(id_paciente) REFERENCES paciente(id_paciente)" +
            ");";


    public static final String CREAR_TABLA_CITA = "CREATE TABLE cita (" +
            "id_cita INTEGER PRIMARY KEY AUTOINCREMENT," +
            "dia TEXT," +
            "hora TEXT," +
            "estado TEXT," +
            "id_personal INTEGER," +
            "id_expediente TEXT," +
            "FOREIGN KEY(id_personal) REFERENCES personal(id_personal)," +
            "FOREIGN KEY(id_expediente) REFERENCES expediente(id_expediente)" +
            ");";

    public static final String CREAR_TABLA_CONSULTA = "CREATE TABLE consulta (" +
            "id_consulta INTEGER PRIMARY KEY AUTOINCREMENT," +
            "dia TEXT," +
            "hora TEXT," +
            "diagnostico TEXT," +
            "tratamiento TEXT" +
            "estado INTEGER," +
            "id_cita INTEGER," +
            "FOREIGN KEY(id_cita) REFERENCES cita(id_cita)" +
            ");";


    public static final String CREAR_TABLA_SOLICITUDES = "CREATE TABLE solicitudes (" +
            "id_solicitud INTEGER PRIMARY KEY AUTOINCREMENT," +
            "dia TEXT," +
            "hora TEXT," +
            "sintoma TEXT," +
            "estado INTEGER," +
            "id_expediente INTEGER," +
            "FOREIGN KEY(id_expediente) REFERENCES expediente(id_expediente)" +
            ");";
}
