package com.example.clinicautec.Modelo;

public class Solicitudes {

    private int idSolicitud;
    private String dia;
    private String hora;
    private String sintoma;
    private String estado;
    private String mensaje;
    private Expediente expediente;

    public Solicitudes() {
    }

    public Solicitudes(int idSolicitud, String dia, String hora, String sintoma, String estado,String mensaje, Expediente expediente) {
        this.idSolicitud = idSolicitud;
        this.dia = dia;
        this.hora = hora;
        this.sintoma = sintoma;
        this.estado = estado;
        this.mensaje = mensaje;
        this.expediente = expediente;
    }

    public Solicitudes(String dia, String hora, String sintoma, String estado,String mensaje, Expediente expediente) {
        this.dia = dia;
        this.hora = hora;
        this.sintoma = sintoma;
        this.estado = estado;
        this.mensaje = mensaje;
        this.expediente = expediente;
    }

    public int getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getSintoma() {
        return sintoma;
    }

    public void setSintoma(String sintoma) {
        this.sintoma = sintoma;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    @Override
    public String toString() {
        return "Solicitudes{" +
                "idSolicitud=" + idSolicitud +
                ", dia='" + dia + '\'' +
                ", hora='" + hora + '\'' +
                ", sintoma='" + sintoma + '\'' +
                ", estado=" + estado +
                ", expediente=" + expediente +
                '}';
    }
}
