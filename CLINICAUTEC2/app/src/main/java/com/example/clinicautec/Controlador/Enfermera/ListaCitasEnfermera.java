package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.DAO.CitasDao;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaCitasEnfermera extends AppCompatActivity {

    Cita cita;
    Personal personal;
    Expediente expediente;
    Paciente paciente;


    FloatingActionButton flbuton;
    TableLayout tblListaCita;
    CitasDao citaD;

    ListView lv;
    ArrayList<Cita> listaCitas;
    ArrayList<String> lista;
    RecordListCitasE adaptador = null;

    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_citas_enfermera);
        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        cita = new Cita();
        personal = new Personal();
        expediente = new Expediente();
        paciente = new Paciente();

        citaD = new CitasDao(getApplicationContext());

        lv = (ListView)findViewById(R.id.lista);

        //listaCitas = new ArrayList<>();
        //listaCitas = citaD.getCitas();

        //adaptador = new RecordListCitasE(this, R.layout.activity_row_citas, listaCitas);

        //lv.setAdapter(adaptador);
        //lista.clear();
        listaCita();

        //System.out.println("LEGOOOOAQUI 2: " +listaCitas.size());
        //adaptador.notifyDataSetChanged();

        flbuton = findViewById(R.id.floatingActionButton);
        flbuton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objAgregarCita = new Intent(getApplicationContext(), AgregarCitas.class);
                startActivity(objAgregarCita);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void listaCita(){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/cita/read.php";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                lista = new ArrayList<>();
                listaCitas = new ArrayList<>();

                try {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for(int i=0; i < mJsonArray.length(); i++){
                        JSONObject mJSONObject = mJsonArray.getJSONObject(i);

                        cita = new Cita();
                        personal = new Personal();
                        expediente = new Expediente();
                        paciente = new Paciente();

                        cita.setIdCita(mJSONObject.getInt("id_cita"));
                        cita.setDia(mJSONObject.getString("dia"));
                        cita.setHora(mJSONObject.getString("hora"));
                        personal.setNombre(mJSONObject.getString("nombre"));
                        expediente.setIdExpediente(mJSONObject.getString("expediente"));
                        paciente.setNombre(mJSONObject.getString("paciente_nombre"));
                        paciente.setApellido(mJSONObject.getString("paciente_apellido"));

                        cita.setExpediente(expediente);
                        cita.setPersonal(personal);
                        expediente.setPaciente(paciente);
                        cita.setExpediente(expediente);

                        listaCitas.add(cita);

                    }
                }catch (JSONException e){
                    e.printStackTrace();
                    System.out.println("Error CATCH Lista Cita: " +e.getMessage());
                }

                adaptador = new RecordListCitasE(ListaCitasEnfermera.this, R.layout.activity_row_citas, listaCitas);
                lv.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error Lista Cita: " +error.getMessage());
                Toast.makeText(getApplicationContext(), "Error" +error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        queue.add(request);
    }
}
