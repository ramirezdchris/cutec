package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.clinicautec.DAO.ExpedienteDao;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.R;

import java.util.ArrayList;

public class MenuEnfermera extends AppCompatActivity {

    CardView cdvCitas, cdvExpediente, cdvDoctores, cdvSolicitudes, cdvPacientes;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_enfermera);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        cdvCitas = findViewById(R.id.cdvCitasE);
        cdvExpediente = findViewById(R.id.cdvExpedientesE);
        //cdvDoctores = findViewById(R.id.cdvDoctoresE);
        cdvSolicitudes = findViewById(R.id.cdvSolicitudE);
        cdvPacientes = findViewById(R.id.cdvPacientesE);

        session = getSharedPreferences("session", Context.MODE_PRIVATE);
        String id = session.getString("id", "");
        String nombre = session.getString("nombre", "");
        String apellido = session.getString("apellido", "");
        int rol = session.getInt("rol", 0);
        //Toast.makeText(getApplicationContext(), "Nombre " +nombre +" ID: " +rol, Toast.LENGTH_LONG).show();

        cdvCitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objEnfermera = new Intent(getApplicationContext(), ListaCitasEnfermera.class);
                startActivity(objEnfermera);

            }
        });

        cdvExpediente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objEnfermera = new Intent(getApplicationContext(), listaExpedienteEnfermera.class);
                startActivity(objEnfermera);
            }
        });


        cdvSolicitudes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objEnfemera = new Intent(getApplicationContext(), SolicitudesPacientes.class);
                startActivity(objEnfemera);
            }
        });

        cdvPacientes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objEnfemera = new Intent(getApplicationContext(), ListaPacientes.class);
                startActivity(objEnfemera);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
