package com.example.clinicautec.Controlador.Doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Consulta;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class VerExpedienteDoctor extends AppCompatActivity {

    EditText txvHistorialdiagnostico,txvhHistorialtratamiento;
    TextView txvcarnetexpeHis,tvfechaHistorial,tvhoraHistorial;
    Button btnterminarHis;

    ArrayList<String> lista;
    ArrayList<Consulta> lista2;
    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_expediente_doctor);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txvHistorialdiagnostico = (EditText)findViewById(R.id.txvHistorialdiagnostico2);
        txvhHistorialtratamiento = (EditText)findViewById(R.id.txvhHistorialtratamiento2);
        txvcarnetexpeHis = (TextView) findViewById(R.id.txvcarnetexpeHis2);
        tvfechaHistorial = (TextView) findViewById(R.id.tvfechaHistorial2);
        tvhoraHistorial = (TextView) findViewById(R.id.tvhoraHistorial2);
        btnterminarHis = (Button) findViewById(R.id.btnterminarHis2);

        queue = Volley.newRequestQueue(getApplicationContext());

        String IdConsulta = getIntent().getStringExtra("Idconsulta");
        String Expediente = getIntent().getStringExtra("expedientee");

        txvcarnetexpeHis.setText(""+Expediente);
        txvHistorialdiagnostico.setInputType(InputType.TYPE_NULL);
        txvhHistorialtratamiento.setInputType(InputType.TYPE_NULL);
        VerExpedienteDoct(IdConsulta);

        btnterminarHis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objExpediente = new Intent(getApplicationContext(), MenuDoctor.class);
                startActivity(objExpediente);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void VerExpedienteDoct(String idConsulta) {
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/consulta/read_single.php?id=" + idConsulta;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                lista2 = new ArrayList<>();
                lista = new ArrayList<>();
                try{
                    JSONArray json=response.getJSONArray("data");
                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(0);

                        Consulta consulta = new Consulta();
                        consulta.setIdConsulta(jsonObject.optInt("id_consulta"));
                        consulta.setDia(jsonObject.optString("dia"));
                        consulta.setHora(jsonObject.optString("hora"));
                        consulta.setDiagnostico(jsonObject.optString("diagnostico"));
                        consulta.setTratamiento(jsonObject.optString("tratamiento"));
                        consulta.setEstado(jsonObject.optString("estado"));
                        //consulta.setIdConsulta(jsonObject.optInt("id_cita"));
                        lista2.add(consulta);
                        lista.add(jsonObject.getString("id_consulta") +" " +jsonObject.getString("dia") + " " +jsonObject.getString("hora") + " " +jsonObject.getString("diagnostico") + " " +jsonObject.getString("tratamiento")+ " " +jsonObject.getString("estado")+ " " +jsonObject.getString("id_cita"));

                    String Fecha,Hora,Diagnostico,Tratamiento;
                    //Consulta c= new Consulta();
                    Fecha = consulta.getDia();
                    Hora = consulta.getHora();
                    Diagnostico = consulta.getDiagnostico();
                    Tratamiento = consulta.getTratamiento();
                    tvfechaHistorial.setText("Fecha : "+Fecha);
                    tvhoraHistorial.setText("Hora : "+Hora);
                    txvHistorialdiagnostico.setText(""+Diagnostico);
                    txvhHistorialtratamiento.setText(""+Tratamiento);

                }catch (Exception e){
                    e.printStackTrace();
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No hay Datos Registrados", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" + error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " " + error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }
}
