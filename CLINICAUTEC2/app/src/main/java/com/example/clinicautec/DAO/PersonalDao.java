package com.example.clinicautec.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.Util.ConexionSQLiteHelper;

import java.util.ArrayList;

public class PersonalDao {

    ConexionSQLiteHelper conn;
    SQLiteDatabase db;

    public PersonalDao(Context context){
        conn = new ConexionSQLiteHelper(context, "db", null, 1);

    }

    public void openDB(){
        db = conn.getWritableDatabase(); // Abrimos la base de datos para editarla
    }

    public void closeDB(){
        conn.close();
        db.close();
    }

    public boolean insertar(Personal personal){
        try {
            ContentValues values = new ContentValues();
            values.put("nombre", personal.getNombre());
            values.put("apellido", personal.getApellido());
            values.put("dui", personal.getDui());
            values.put("correo", personal.getCorreo());
            values.put("telefono", personal.getTelefono());
            db.insert("personal", null, values);
            return true;
        }catch (Exception e){
            System.err.println("Error Metodo Insertar Personal Dao" +e.getMessage());
            return false;
        }
    }

    public boolean modificar(Personal personal){
        try {
            ContentValues values = new ContentValues();
            values.put("nombre", personal.getNombre());
            values.put("apellido", personal.getApellido());
            values.put("dui", personal.getDui());
            values.put("correo", personal.getCorreo());
            values.put("telefono", personal.getTelefono());
            db.update("personal", values, "id=?", new String[]{personal.getIdPersonal() +""});
            return true;
        }catch (Exception e){
            System.err.println("Error Metodo Modificar PersonalDao" +e.getMessage());
            return false;
        }
    }

    public boolean eliminar(int id){
        try {

            db.delete("personal", "id=" +id, null);
            return true;
        }catch (Exception e){
            System.err.println("Error Metodo Eliminar PersonalDao" +e.getMessage());
            return false;
        }
    }

    public ArrayList<Personal> getPersonal(){
        ArrayList<Personal> listaPersonal = new ArrayList<>();
        try {
            Cursor c = db.rawQuery("SELECT * FROM personal" , null);

            while(c.moveToNext()){
                Personal personal = new Personal();
                personal.setIdPersonal(c.getInt(0));
                personal.setNombre(c.getString(1));
                personal.setApellido(c.getString(2));
                personal.setDui(c.getString(3));
                personal.setCorreo(c.getString(4));
                personal.setTelefono(c.getString(5));
                listaPersonal.add(personal);
            }
            return listaPersonal;
        }catch (Exception e){
            System.err.println("Error Metodo getPersonal PersonalDao" +e.getMessage());
            return null;
        }
    }


}
