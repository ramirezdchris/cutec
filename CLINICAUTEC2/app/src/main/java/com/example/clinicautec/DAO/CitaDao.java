package com.example.clinicautec.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.Util.ConexionSQLiteHelper;

import java.util.ArrayList;

public class CitaDao {

    ConexionSQLiteHelper conn;
    SQLiteDatabase db;
    public CitaDao(Context context){
        conn = new ConexionSQLiteHelper(context, null , null, 1);
    }

    public void closeDB(){
        conn.close();
        db.close();
    }

    public  ArrayList<Cita> listacita(String expe){
        ArrayList<Cita> lista = new ArrayList<>();
        try{
            Cursor cursor = conn.getData("SELECT * FROM cita where id_expediente="+expe);
            while(cursor.moveToNext()){
                Cita c = new Cita();
                c.setIdCita(cursor.getInt(0));
                c.setDia(cursor.getString(1));
                c.setHora(cursor.getString(2));
                lista.add(c);
            }

            return lista;
        }catch (Exception e){
            System.err.println("Error Metodo getExpediente2 ExpedienteDao " +e.getMessage());
            return  null;
        }
    }

    public ArrayList<Cita> getCitashistorial(String expediente){
        ArrayList<Cita> listacita = new ArrayList<>();
        try {
            Cursor cursor = conn.getData("SELECT c.id_cita,c.dia,c.hora,e.id_expediente,p.id_personal,p.nombre,c.id_expediente ,c.id_personal" +
                    " FROM cita as c INNER JOIN expediente as e on c.id_expediente = e.id_expediente " +
                    " INNER JOIN personal as p on c.id_personal = p.id_personal where c.id_expediente ="+expediente);

            while(cursor.moveToNext()){
                Cita c = new Cita();
                c.setIdCita(cursor.getInt(0));
                c.setDia(cursor.getString(1));
                c.setHora(cursor.getString(2));

                Expediente e = new Expediente();
                e.setIdExpediente(cursor.getString(3));

                Personal p = new Personal();
                p.setIdPersonal(cursor.getInt(4));
                p.setNombre(cursor.getString(5));

                c.setExpediente(e);
                c.setPersonal(p);
                listacita.add(c);
            }
            System.out.println();
             return listacita;
        }catch (Exception e){
            System.err.println("Error Metodo getCitashistorial CitaDao " +e.getMessage());
            return null;
        }
    }
}
