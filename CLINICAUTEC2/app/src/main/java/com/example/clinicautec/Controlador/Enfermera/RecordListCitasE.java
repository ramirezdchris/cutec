package com.example.clinicautec.Controlador.Enfermera;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Solicitudes;
import com.example.clinicautec.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class RecordListCitasE extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Cita> recordListCita;
    private RequestQueue queue;

    public RecordListCitasE(Context context, int layout, ArrayList<Cita> recordListCita){
        this.context = context;
        this.layout = layout;
        this.recordListCita = recordListCita;
        this.queue = Volley.newRequestQueue(context);
    }

    @Override
    public int getCount() {
        return recordListCita.size();
    }

    @Override
    public Object getItem(int position) {
        return recordListCita.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView tvExpediente, tvPaciente, tvHora, tvFecha, tvDoctor;
        Button btnActualizarCita, btnEliminarCita;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = new ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.tvExpediente = row.findViewById(R.id.tvExpedienteRow);
            holder.tvPaciente = row.findViewById(R.id.tvPacienteCom);
            holder.tvHora = row.findViewById(R.id.tvHoraCom);
            holder.tvFecha = row.findViewById(R.id.tvFechaCom);
            holder.tvDoctor = row.findViewById(R.id.tvDoctorCom);

            holder.btnActualizarCita = row.findViewById(R.id.btnActualizarCita);
            holder.btnEliminarCita = row.findViewById(R.id.btnEliminarCita);
            row.setTag(holder);
        }else{
            holder = (ViewHolder)row.getTag();
        }

        final Cita model = recordListCita.get(position);

        holder.tvExpediente.setText(model.getExpediente().getIdExpediente());
        holder.tvPaciente.setText(model.getExpediente().getPaciente().getNombre() + " " +model.getExpediente().getPaciente().getApellido());
        holder.tvHora.setText(model.getHora());
        holder.tvFecha.setText(model.getDia());
        holder.tvDoctor.setText(model.getPersonal().getNombre());

        holder.btnActualizarCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogActualizar(model);
            }
        });

        holder.btnEliminarCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogEliminar(model);
            }
        });

        return row;
    }

    private void mostrarDialogEliminar(final Cita cita){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);

        dialog.setTitle("Eliminar Cita");
        dialog.setMessage("¿Esta seguro de eliminar esta cita?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                elimiarCita(cita);
            }
        });

        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.create();
        dialog.show();
    }

    private void mostrarDialogActualizar(final Cita model){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        AlertDialog.Builder dialog = new AlertDialog.Builder(this.context);

        dialog.setTitle("Actualizar Cita");
        dialog.setMessage("¿Desea actualizar esta cita?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent objActualizarCita = new Intent(context, AgregarCitas.class);
                objActualizarCita.putExtra("id_cita", model.getIdCita());
                objActualizarCita.putExtra("id_expediente", model.getExpediente().getIdExpediente());
                objActualizarCita.putExtra("hora", model.getHora());
                objActualizarCita.putExtra("fecha", model.getDia());
                objActualizarCita.putExtra("opcion", 2);
                objActualizarCita.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(objActualizarCita);

            }
        });

        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.create().show();
    }

    private void elimiarCita(final Cita cita){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/cita/delete.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("nombre", cita.getIdCita());


        }catch (JSONException e){
            System.out.println("ERRRRORRRR EN EL CATCH Agregar Paciente: " +e.getMessage());
            Toast.makeText(context, "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(context, response.getString("mensaje"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);

    }
}
