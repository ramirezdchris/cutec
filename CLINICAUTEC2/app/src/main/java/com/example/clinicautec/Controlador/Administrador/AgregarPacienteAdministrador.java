package com.example.clinicautec.Controlador.Administrador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Enfermera.AgregarPacientes;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.R;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

public class AgregarPacienteAdministrador extends AppCompatActivity {

    Button btnAgregarPacienteAd;
    EditText edtNombrePacienteAd, edtApellidoPacienteAd, edtCarnetAd, edtTelefonoAd, edtFechaNacimientoAd, edtCorreoAd;
    EditText edtDuiAd;
    Switch switch1Ad;

    Paciente paciente;

    int id_paciente;
    String pass;

    RequestQueue queue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_paciente_administrador);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        edtNombrePacienteAd = findViewById(R.id.edtNombrePacienteAd);
        edtApellidoPacienteAd = findViewById(R.id.edtApellidoPacienteAd);
        edtDuiAd = findViewById(R.id.edtDuiAd);
        switch1Ad = findViewById(R.id.switch1Ad);
        edtCarnetAd = findViewById(R.id.edtCarnetAd);
        edtTelefonoAd = findViewById(R.id.edtTelefonoAd);
        edtFechaNacimientoAd = findViewById(R.id.edtFechaNacimientoAd);
        edtCorreoAd = findViewById(R.id.edtCorreoAd);
        btnAgregarPacienteAd = findViewById(R.id.btnAgregarPacienteAd);


        edtCarnetAd.setVisibility(View.INVISIBLE);
        // Mask
        SimpleMaskFormatter smfDui = new SimpleMaskFormatter("NNNNNNNN-N");
        MaskTextWatcher mtwDui = new MaskTextWatcher(edtDuiAd, smfDui);
        SimpleMaskFormatter smfCarnet = new SimpleMaskFormatter("NN-NNNN-NNNN");
        MaskTextWatcher mtwCarnet = new MaskTextWatcher(edtCarnetAd, smfCarnet);
        SimpleMaskFormatter smfTelefono = new SimpleMaskFormatter("NNNN-NNNN");
        MaskTextWatcher mtwTelefono = new MaskTextWatcher(edtTelefonoAd, smfTelefono);

        // Aplicar mascaras
        edtDuiAd.addTextChangedListener(mtwDui);
        edtCarnetAd.addTextChangedListener(mtwCarnet);
        edtTelefonoAd.addTextChangedListener(mtwTelefono);

        //Objetos
        paciente = new Paciente();

        switch1Ad.setTextOff("Si");
        switch1Ad.setTextOn("No");
        edtCarnetAd.setText("00-0000-0000");
        switch1Ad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(switch1Ad.isChecked()){
                    edtCarnetAd.setVisibility(View.VISIBLE);
                    edtCarnetAd.setText("");
                }else{
                    edtCarnetAd.setVisibility(View.INVISIBLE);
                    edtCarnetAd.setText("No carnet");
                }

            }
        });

        edtFechaNacimientoAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AgregarPacienteAdministrador.this,R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        final int mesActual = month + 1;
                        String diaFormateado = (dayOfMonth < 10) ? "0" +String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                        String mesFormateado = (mesActual < 10) ? "0" +String.valueOf(mesActual) : String.valueOf(mesActual);
                        edtFechaNacimientoAd.setText(year + "-" +mesFormateado +"-" +diaFormateado);
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });




        btnAgregarPacienteAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Obteniendo datos

                String nombre;
                String apellido;
                String dui;
                String carnet;
                String telefono;
                String fecha_nacimiento;
                String correo;

                nombre = edtNombrePacienteAd.getText().toString().trim();
                apellido = edtApellidoPacienteAd.getText().toString().trim();
                dui = edtDuiAd.getText().toString().trim();
                carnet = edtCarnetAd.getText().toString().trim();
                telefono = edtTelefonoAd.getText().toString().trim();
                fecha_nacimiento = edtFechaNacimientoAd.getText().toString().trim();
                correo = edtCorreoAd.getText().toString().trim();

                if(TextUtils.isEmpty(nombre)){
                    edtNombrePacienteAd.setError("Digite el nombre del paciente");
                    edtNombrePacienteAd.requestFocus();
                }else if(TextUtils.isEmpty(apellido)){
                    edtApellidoPacienteAd.setError("Digite el apellido del paciente");
                    edtApellidoPacienteAd.requestFocus();
                }else if(TextUtils.isEmpty(dui)){
                    edtDuiAd.setError("Digite el DUI del paciente");
                    edtDuiAd.requestFocus();
                }else if(TextUtils.isEmpty(telefono)){
                    edtTelefonoAd.setError("Digite el telefono");
                    edtTelefonoAd.requestFocus();
                }else if(TextUtils.isEmpty(fecha_nacimiento)){
                    edtFechaNacimientoAd.setError("Seleccion un fecha de nacimiento");
                    edtFechaNacimientoAd.requestFocus();
                }else if(TextUtils.isEmpty(correo)){
                    edtCorreoAd.setError("Digite el correo electronico");
                    edtCorreoAd.requestFocus();
                }else if(TextUtils.isEmpty(carnet)){
                    edtCarnetAd.setError("Digite el carnet del paciente");
                    edtCarnetAd.requestFocus();
                }else{
                    boolean esCorreo = Patterns.EMAIL_ADDRESS.matcher(correo).matches();
                    if(!esCorreo){
                        edtCorreoAd.setError("Digite un correo valido");
                        edtCorreoAd.requestFocus();
                    }else if(dui.length() < 10){
                        edtDuiAd.setError("El dui tiene que contener 10 digitos");
                        edtDuiAd.requestFocus();
                    }else if(telefono.length() < 9){
                        edtTelefonoAd.setError("Numero telefonico debe contener 8 digitos");
                        edtTelefonoAd.requestFocus();
                    }else if(carnet.length() < 12){
                        edtCarnetAd.setError("Numero de carnet debe contener 12 caracteres");
                        edtCarnetAd.requestFocus();
                    }
                    else{
                        paciente.setNombre(nombre);
                        paciente.setApellido(apellido);
                        paciente.setDui(dui);
                        paciente.setCarnet(carnet);
                        paciente.setTelefono(telefono);
                        paciente.setFechaNacimiento(fecha_nacimiento);
                        paciente.setCorreo(correo);
                        paciente.setPass(dui);
                        Date date = new Date();
                        System.out.println("Dia: " +date.getDay() +" Mes: " +date.getMonth() +" Año: " +date.getYear());

                        String opcion = btnAgregarPacienteAd.getText().toString().trim();


                            mostrarDialog(paciente);



                    }

                }
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void mostrarDialog(final Paciente paciente){
        AlertDialog.Builder builder = new AlertDialog.Builder(AgregarPacienteAdministrador.this);
        //AlertDialog.Builder builder = new AlertDialog.Builder(RecordListPacientes.this.context);
        //LayoutInflater la = getLayoutInflater();
        builder.setTitle("Agregar Paciente");
        builder.setMessage("¿Quieres agregar a " +paciente.getNombre() +" " +paciente.getApellido() +"?").setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //citasD = new CitasDao(getApplicationContext());
                String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/paciente/create.php";

                JSONObject jsonObject = null;
                Map<String,String> params = new Hashtable<String,String>();

                try {
                    jsonObject = new JSONObject();
                    jsonObject.put("nombre", paciente.getNombre());
                    jsonObject.put("apellido" , paciente.getApellido());
                    jsonObject.put("dui" , paciente.getDui());
                    jsonObject.put("carnet" , paciente.getCarnet());
                    jsonObject.put("correo" , paciente.getCorreo());
                    jsonObject.put("telefono" , paciente.getTelefono());
                    jsonObject.put("fecha_nacimiento" , paciente.getFechaNacimiento());
                    jsonObject.put("pass" , paciente.getPass());

                    edtNombrePacienteAd.setText("");
                    edtApellidoPacienteAd.setText("");
                    edtDuiAd.setText("");
                    edtTelefonoAd.setText("");
                    edtFechaNacimientoAd.setText("");
                    edtCorreoAd.setText("");

                }catch (JSONException e){
                    System.out.println("ERRRRORRRR EN EL CATCH Agregar Paciente: " +e.getMessage());
                    Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(getApplicationContext(), "Se inserto correctamente", Toast.LENGTH_LONG).show();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                        //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }){

                };

                queue.add(request);

            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "No se realizo la cita ", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).show();
    }

}