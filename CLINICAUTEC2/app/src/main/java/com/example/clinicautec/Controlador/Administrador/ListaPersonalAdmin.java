package com.example.clinicautec.Controlador.Administrador;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Enfermera.ListaCitasEnfermera;
import com.example.clinicautec.Controlador.Enfermera.RecordListCitasE;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaPersonalAdmin extends AppCompatActivity {

    ListView listaPersonal;

    RequestQueue queue;
    Personal personal;
    RecordListPersonal adaptador;

    ArrayList<String> lista = new ArrayList<>();
    ArrayList<Personal> listaP = new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_personal_admin);


        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());
        listaPersonal = (ListView) findViewById(R.id.lvListadoP);

        personal = new Personal();
        listadoPersonal();
    }

    private void listadoPersonal(){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/personal/read.php";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                lista = new ArrayList<>();
                listaP = new ArrayList<>();

                try {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for(int i=0; i < mJsonArray.length(); i++){
                        JSONObject mJSONObject = mJsonArray.getJSONObject(i);

                        personal = new Personal();

                        personal.setIdPersonal(mJSONObject.getInt("id_personal"));
                        personal.setNombre(mJSONObject.getString("nombre"));
                        personal.setApellido(mJSONObject.getString("apellido"));
                        personal.setDui(mJSONObject.getString("dui"));
                        personal.setCorreo(mJSONObject.getString("correo"));
                        personal.setTelefono(mJSONObject.getString("telefono"));

                        listaP.add(personal);

                    }
                }catch (JSONException e){
                    e.printStackTrace();
                    System.out.println("Error CATCH Lista Cita: " +e.getMessage());
                }

                adaptador = new RecordListPersonal(ListaPersonalAdmin.this, R.layout.activity_row_personal, listaP);
                listaPersonal.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error Lista Persoanl: " +error.getMessage());
                Toast.makeText(getApplicationContext(), "Error" +error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        queue.add(request);
    }
}
