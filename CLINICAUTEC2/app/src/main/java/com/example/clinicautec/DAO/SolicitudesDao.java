package com.example.clinicautec.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.clinicautec.Util.ConexionSQLiteHelper;

public class SolicitudesDao {

    ConexionSQLiteHelper conn;
    SQLiteDatabase db;

    public SolicitudesDao(Context context){
        conn = new ConexionSQLiteHelper(context, "db", null, 1);
    }

    public void openDB(){
        db = conn.getWritableDatabase(); // Abrimos la base de datos para editarla
    }

    public void closeDB(){
        conn.close();
        db.close();
    }
}
