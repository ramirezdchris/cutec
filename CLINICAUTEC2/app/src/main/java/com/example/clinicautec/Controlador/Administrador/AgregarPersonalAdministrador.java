package com.example.clinicautec.Controlador.Administrador;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Doctor.ExpedienteDoctor;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Consulta;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.Modelo.Rol;
import com.example.clinicautec.Modelo.Usuario;
import com.example.clinicautec.R;
import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class AgregarPersonalAdministrador extends AppCompatActivity {

    EditText edtnombreAd2, edtapellidoAd2, edtduiAd2, edtcorreoAd2, edtTelefonoAd2, edtusuaarioAd2, edtpassAd2;
    RadioButton rdbenfermera, rdbdoctor;
    Button btnagregarP;

    Personal personal;
    Usuario usu;
    Rol rol;

    RequestQueue queue;
    int idPersonal = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_personal_administrador);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        edtnombreAd2 = findViewById(R.id.edtnombreAd2);
        edtapellidoAd2 = findViewById(R.id.edtapellidoAd2);
        edtduiAd2 = findViewById(R.id.edtduiAd2);
        edtcorreoAd2 = findViewById(R.id.edtcorreoAd2);
        edtTelefonoAd2 = findViewById(R.id.edtTelefonoAd2);
        edtusuaarioAd2 = findViewById(R.id.edtusuaarioAd2);
        edtpassAd2 = findViewById(R.id.edtpassAd2);
        rdbenfermera = findViewById(R.id.rdbenfermera);
        rdbdoctor = findViewById(R.id.rdbdoctor);
        btnagregarP = findViewById(R.id.btnagregarP);

        personal = new Personal();
        usu = new Usuario();
        rol = new Rol();

        SimpleMaskFormatter smfDui = new SimpleMaskFormatter("NNNNNNNN-N");
        MaskTextWatcher mtwDui = new MaskTextWatcher(edtduiAd2, smfDui);
        SimpleMaskFormatter smfTelefono = new SimpleMaskFormatter("NNNN-NNNN");
        MaskTextWatcher mtwTelefono = new MaskTextWatcher(edtTelefonoAd2, smfTelefono);

        final int opcion = getIntent().getIntExtra("opcion", 0);
        if (opcion == 1) {
            String nombre, apellido, dui, correo, telefono;

            idPersonal = getIntent().getIntExtra("id_personal", 0);
            nombre = getIntent().getStringExtra("nombreP");
            apellido = getIntent().getStringExtra("apellidoP");
            dui = getIntent().getStringExtra("duiP");
            correo = getIntent().getStringExtra("correoP");
            telefono = getIntent().getStringExtra("telefonoP");
            edtnombreAd2.setText(nombre);
            edtapellidoAd2.setText(apellido);
            edtduiAd2.setText(dui);
            edtcorreoAd2.setText(correo);
            edtTelefonoAd2.setText(telefono);
            btnagregarP.setText("Actualizar Paciente");
            edtusuaarioAd2.setVisibility(View.INVISIBLE);
            edtpassAd2.setVisibility(View.INVISIBLE);
            rdbdoctor.setVisibility(View.INVISIBLE);
            rdbenfermera.setVisibility(View.INVISIBLE);
        }

        edtduiAd2.addTextChangedListener(mtwDui);
        edtTelefonoAd2.addTextChangedListener(mtwTelefono);

        btnagregarP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (opcion == 1) {
                    String nombre = edtnombreAd2.getText().toString().trim();
                    String apellido = edtapellidoAd2.getText().toString().trim();
                    String dui = edtduiAd2.getText().toString().trim();
                    String correo = edtcorreoAd2.getText().toString().trim();
                    String telefono = edtTelefonoAd2.getText().toString().trim();
                    String usuario = edtusuaarioAd2.getText().toString().trim();
                    String pass = edtpassAd2.getText().toString().trim();

                    if (TextUtils.isEmpty(nombre)) {
                        edtnombreAd2.setError("El campo no puede quedar vacio");
                        edtnombreAd2.requestFocus();
                    } else if (TextUtils.isEmpty(apellido)) {
                        edtapellidoAd2.setError("El campo no puede quedar vacio");
                        edtapellidoAd2.requestFocus();
                    } else if (TextUtils.isEmpty(dui)) {
                        edtduiAd2.setError("El campo no puede quedar vacio");
                        edtduiAd2.requestFocus();
                    } else if (TextUtils.isEmpty(correo)) {
                        edtcorreoAd2.setError("El campo no puede quedar vacio");
                        edtcorreoAd2.requestFocus();
                    } else if (TextUtils.isEmpty(telefono)) {
                        edtTelefonoAd2.setError("El campo no puede quedar vacio");
                        edtTelefonoAd2.requestFocus();
                    } else {
                        if (btnagregarP.getText().toString().equals("Actualizar Paciente")) {
                            personal.setIdPersonal(idPersonal);
                            personal.setNombre(nombre);
                            personal.setApellido(apellido);
                            personal.setDui(dui);
                            personal.setCorreo(correo);
                            personal.setTelefono(telefono);
                            actualizarPersonal(personal);
                        }
                    }
                } else {
                    String nombre = edtnombreAd2.getText().toString().trim();
                    String apellido = edtapellidoAd2.getText().toString().trim();
                    String dui = edtduiAd2.getText().toString().trim();
                    String correo = edtcorreoAd2.getText().toString().trim();
                    String telefono = edtTelefonoAd2.getText().toString().trim();
                    String usuario = edtusuaarioAd2.getText().toString().trim();
                    String pass = edtpassAd2.getText().toString().trim();

                    if (TextUtils.isEmpty(nombre)) {
                        edtnombreAd2.setError("El campo no puede quedar vacio");
                        edtnombreAd2.requestFocus();
                    } else if (TextUtils.isEmpty(apellido)) {
                        edtapellidoAd2.setError("El campo no puede quedar vacio");
                        edtapellidoAd2.requestFocus();
                    } else if (TextUtils.isEmpty(dui)) {
                        edtduiAd2.setError("El campo no puede quedar vacio");
                        edtduiAd2.requestFocus();
                    } else if (TextUtils.isEmpty(correo)) {
                        edtcorreoAd2.setError("El campo no puede quedar vacio");
                        edtcorreoAd2.requestFocus();
                    } else if (TextUtils.isEmpty(telefono)) {
                        edtTelefonoAd2.setError("El campo no puede quedar vacio");
                        edtTelefonoAd2.requestFocus();
                    } else if (TextUtils.isEmpty(usuario)) {
                        edtusuaarioAd2.setError("El campo no puede quedar vacio");
                        edtusuaarioAd2.requestFocus();
                    } else if (TextUtils.isEmpty(pass)) {
                        edtpassAd2.setError("El campo no puede quedar vacio");
                        edtpassAd2.requestFocus();
                    } else {

                        if (rdbenfermera.isChecked() == true) {
                            int idrol = 2;
                            personal.setNombre(nombre);
                            personal.setApellido(apellido);
                            personal.setDui(dui);
                            personal.setCorreo(correo);
                            personal.setTelefono(telefono);
                            rol.setIdRol(idrol);
                            usu.setUsuario(usuario);
                            usu.setPass(pass);
                            usu.setRol(rol);

                            InsertarPersonal(personal, usu);

                        } else if (rdbdoctor.isChecked() == true) {

                            int idrol = 3;
                            personal.setNombre(nombre);
                            personal.setApellido(apellido);
                            personal.setDui(dui);
                            personal.setCorreo(correo);
                            personal.setTelefono(telefono);
                            rol.setIdRol(idrol);
                            usu.setUsuario(usuario);
                            usu.setPass(pass);
                            usu.setRol(rol);

                            InsertarPersonal(personal, usu);

                        }
                    }
                }


            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void InsertarPersonal(final Personal personal, final Usuario usu){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(AgregarPersonalAdministrador.this);
        dialogo1.setTitle("Importante");
        dialogo1.setMessage("¿Desea agregar a la nueva Persona?");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptar(personal);

            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelar();
            }
        });
        dialogo1.show();
    }


    public void aceptar(final Personal personal) {

        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/personal/create.php";

        JSONObject jsonObject = null;
        Map<String, String> params = new Hashtable<String, String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("nombre", personal.getNombre());
            jsonObject.put("apellido", personal.getApellido());
            jsonObject.put("dui", personal.getDui());
            jsonObject.put("correo", personal.getCorreo());
            jsonObject.put("telefono", personal.getTelefono());
            edtnombreAd2.setText("");
            edtapellidoAd2.setText("");
            edtduiAd2.setText("");
            edtcorreoAd2.setText("");
            edtTelefonoAd2.setText("");
            edtusuaarioAd2.setText("");
            edtpassAd2.setText("");


        } catch (JSONException e) {
            System.out.println("*********ERROR GRAVE1************");
            e.printStackTrace();
            System.out.println("**********************");
            System.out.println("ERROR AL INSETAR LOS DATOS: " + e.getMessage());
            Toast.makeText(getApplicationContext(), "Hubo un error inesperedo al insertar los datos", Toast.LENGTH_LONG).show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Datos Correctamente Insertados", Toast.LENGTH_LONG).show();
                InsertarUsuario(usu);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor Al Insertar" + error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " " + error, Toast.LENGTH_LONG).show();
            }
        }) {

        };

        queue.add(request);
    }

    public void cancelar() {
        Toast t = Toast.makeText(getApplicationContext(), "Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }

    public void InsertarUsuario(final Usuario usuario){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/usuario/create.php";

        JSONObject jsonObject = null;
        Map<String, String> params = new Hashtable<String, String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("usuario", usuario.getUsuario());
            jsonObject.put("pass", usuario.getPass());
            jsonObject.put("rol", usuario.getRol().getIdRol());

        } catch (JSONException e) {
            System.out.println("*********ERROR GRAVE2************");
            e.printStackTrace();
            System.out.println("**********************");
            System.out.println("ERROR AL actualizarrr LOS DATOS: " + e.getMessage());
            //Toast.makeText(getApplicationContext(), "Hubo un error  al Actualizar los datos", Toast.LENGTH_LONG).show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Toast.makeText(getApplicationContext(), "Usuario Datos Correctamente Inserttado", Toast.LENGTH_LONG).show();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor Al Actualizar" + error.toString());
                //Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                //       " "+error, Toast.LENGTH_LONG).show();
            }
        }) {

        };

        queue.add(request);
    }


    public void actualizarPersonal(final Personal personal) {
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/personal/update.php";

        JSONObject jsonObject = null;
        Map<String, String> params = new Hashtable<String, String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("id_personal", personal.getIdPersonal());
            jsonObject.put("nombre", personal.getNombre());
            jsonObject.put("apellido", personal.getApellido());
            jsonObject.put("dui", personal.getDui());
            jsonObject.put("correo", personal.getCorreo());
            jsonObject.put("telefono", personal.getTelefono());

        } catch (JSONException e) {
            System.out.println("*********ERROR GRAVE2************");
            e.printStackTrace();
            System.out.println("**********************");
            System.out.println("ERROR AL actualizarrr LOS DATOS: " + e.getMessage());
            //Toast.makeText(getApplicationContext(), "Hubo un error  al Actualizar los datos", Toast.LENGTH_LONG).show();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Toast.makeText(getApplicationContext(), response.getString("mensaje"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No se pudo procesar la solicitud", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor Al Actualizar" + error.toString());
                //Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                //       " "+error, Toast.LENGTH_LONG).show();
            }
        }) {

        };

        queue.add(request);
    }
}
