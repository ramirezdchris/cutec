package com.example.clinicautec.Util;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class ConexionSQLiteHelper  extends SQLiteOpenHelper {

    //final String CREAR_TABLA_PERSONAL = "CREATE TABLE personal(id_personal INTEGER PRIMARY KEY, nombre TEXT, apellido TEXT, dui TEXT, correo TEXT, telefono TEXT)";

    public ConexionSQLiteHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(Constantes.CREAR_TABLA_PERSONAL);
            db.execSQL(Constantes.CREAR_TABLA_ROL);
            db.execSQL(Constantes.CREAR_TABLA_USUARIO);
            db.execSQL(Constantes.CREAR_TABLA_PACIENTE);
            db.execSQL(Constantes.CREAR_TABLA_EXPEDIENTE);
            db.execSQL(Constantes.CREAR_TABLA_CITA);
            db.execSQL(Constantes.CREAR_TABLA_CONSULTA);
            db.execSQL(Constantes.CREAR_TABLA_SOLICITUDES);
            //db.execSQL(RolDao.CRE);

            db.execSQL("INSERT INTO personal(nombre,apellido,dui,correo,telefono) VALUES('Maria','Torres','00004','maria@gmail.com','2257-7777')");
            db.execSQL("INSERT INTO personal(nombre,apellido,dui,correo,telefono) VALUES('Doris','Aguilar','00005','doris@gmail.com','2257-7777')");
            db.execSQL("INSERT INTO personal(nombre,apellido,dui,correo,telefono) VALUES('Carlos','Chacon','00006','chacon','2257-7777')");
            db.execSQL("INSERT INTO personal(nombre,apellido,dui,correo,telefono) VALUES('Felipe','Sanchez','00007','Felipe@gmail.com','2290-8000')");

            //db.execSQL("INSERT INTO personal(nombre,apellido,dui,correo,telefono) VALUES('Paciente','Paciente','00005','paciente@gmail.com','2257-7777')");

            db.execSQL("INSERT INTO rol(rol) VALUES('Administrador')");
            db.execSQL("INSERT INTO rol(rol) VALUES('Enfermera')");
            db.execSQL("INSERT INTO rol(rol) VALUES('Doctor')");


            db.execSQL("INSERT INTO usuario(usuario,pass,id_personal,id_rol) VALUES ('maria','123',1,1)");
            db.execSQL("INSERT INTO usuario(usuario,pass,id_personal,id_rol) VALUES ('doris','123',2,2)");
            db.execSQL("INSERT INTO usuario(usuario,pass,id_personal,id_rol) VALUES ('carlos','123',3,3)");

            db.execSQL("INSERT INTO paciente(nombre,apellido,dui,telefono,fecha_nacimiento,correo,pass) VALUES ('Carmen','Liliana','123','2257-7777','1997-06-16','carmen@gmail.com','123')");
            db.execSQL("INSERT INTO paciente(nombre,apellido,dui,telefono,fecha_nacimiento,correo,pass) VALUES ('Mario','Felipe','555','2156-9023','1987-02-26','mario@gmail.com','123')");
            db.execSQL("INSERT INTO expediente(id_expediente,fecha_creacion,id_paciente) VALUES('1734172016','03/10/2018',1)");
            db.execSQL("INSERT INTO expediente(id_expediente,fecha_creacion,id_paciente) VALUES('2001342015','13/06/2017',2)");

            db.execSQL("INSERT INTO cita(dia,hora,id_personal,id_expediente) VALUES('02/05/2020','8:00 AM',2,'1734172016')");
            db.execSQL("INSERT INTO cita(dia,hora,id_personal,id_expediente) VALUES('22/04/2020','2:00 PM',3,'1734172016')");
            db.execSQL("INSERT INTO cita(dia,hora,id_personal,id_expediente) VALUES('02/05/2020','10:00 AM',4,'2001342015')");
            System.out.println("Se crearon las tablas y se insertaron los datos");

        }catch(Exception e){
            System.out.println("No se creo la tabla: " +e.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS personal");
        onCreate(db);
    }

    //Cursor para recorrer las consultas de las listas
    public Cursor getData(String sql){
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }
}