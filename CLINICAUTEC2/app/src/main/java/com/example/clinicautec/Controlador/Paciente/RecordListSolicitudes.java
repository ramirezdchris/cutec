package com.example.clinicautec.Controlador.Paciente;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Modelo.Solicitudes;
import com.example.clinicautec.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class RecordListSolicitudes extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Solicitudes> recordListSolicitudes;

    RequestQueue queue;

    public RecordListSolicitudes (Context context, int layout,ArrayList<Solicitudes> recordListSolicitudes){

        this.context = context;
        this.layout = layout;
        this.recordListSolicitudes = recordListSolicitudes;

        queue = Volley.newRequestQueue(context);
    }

    @Override
    public int getCount() {
        return recordListSolicitudes.size();
    }

    @Override
    public Object getItem(int i) {
        return recordListSolicitudes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class VistaRowSolicitudes{
        TextView tvfechadoctor, tvhoradoctor, tvnombredoctor, tvexpedientedoctor;
        Button btneditarSP,btneliminarSP,btnmensajeSP;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View row = view;
        VistaRowSolicitudes holder2 = new VistaRowSolicitudes();
        if(row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder2.tvfechadoctor = row.findViewById(R.id.tvfechadoctor);
            holder2.tvhoradoctor = row.findViewById(R.id.tvhoradoctor);
            holder2.tvnombredoctor = row.findViewById(R.id.tvnombredoctor);
            holder2.tvexpedientedoctor = row.findViewById(R.id.tvexpedientedoctor);
            holder2.btneditarSP = row.findViewById(R.id.btneditarSP);
            holder2.btneliminarSP = row.findViewById(R.id.btneliminarSP);
            holder2.btnmensajeSP = row.findViewById(R.id.btnmensajeSP);
            row.setTag(holder2);
        }
        else {
            holder2 = (RecordListSolicitudes.VistaRowSolicitudes) row.getTag();
        }

        final Solicitudes solicitudes = recordListSolicitudes.get(i);
        final int IdSol = solicitudes.getIdSolicitud();
        final String IdSolicitud = String.valueOf(IdSol);
        String estado = solicitudes.getEstado();
        final String expediente = solicitudes.getExpediente().getIdExpediente();
        final String mensaje = solicitudes.getMensaje();

        if(estado.equals("Aprobado") || estado.equals("Atendido") ){
            holder2.tvfechadoctor.setText(solicitudes.getDia());
            holder2.tvhoradoctor.setText(solicitudes.getHora());
            holder2.tvnombredoctor.setText(solicitudes.getSintoma());
            holder2.tvexpedientedoctor.setText(solicitudes.getEstado());
            //CAMBIAR DE COLOR
            holder2.tvfechadoctor.setTextColor(Color.parseColor("#0F9E15"));
            holder2.tvhoradoctor.setTextColor(Color.parseColor("#0F9E15"));
            holder2.tvnombredoctor.setTextColor(Color.parseColor("#0F9E15"));
            holder2.tvexpedientedoctor.setTextColor(Color.parseColor("#0F9E15"));
            //BOTONES
            holder2.btneditarSP.setVisibility(View.GONE);
            holder2.btneliminarSP.setVisibility(View.GONE);
            holder2.btnmensajeSP.setVisibility(View.GONE);


        }else if(estado.equals("Pendiente")){
            holder2.tvfechadoctor.setText(solicitudes.getDia());
            holder2.tvhoradoctor.setText(solicitudes.getHora());
            holder2.tvnombredoctor.setText(solicitudes.getSintoma());
            holder2.tvexpedientedoctor.setText(solicitudes.getEstado());
            holder2.btnmensajeSP.setVisibility(View.GONE);
            //CAMBIAR DE COLOR
            holder2.tvfechadoctor.setTextColor(Color.parseColor("#00BCD4"));
            holder2.tvhoradoctor.setTextColor(Color.parseColor("#00BCD4"));
            holder2.tvnombredoctor.setTextColor(Color.parseColor("#00BCD4"));
            holder2.tvexpedientedoctor.setTextColor(Color.parseColor("#00BCD4"));
            holder2.btneditarSP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String fechaActualizar = solicitudes.getDia();
                    String horaActualizar = solicitudes.getHora();
                    String sintomaActualizar = solicitudes.getSintoma();
                    Intent objAgregarCita = new Intent(context, AgregarSolicitudModulopaciente.class);
                    objAgregarCita.putExtra("idExpediente", expediente);
                    objAgregarCita.putExtra("IdSolicitud", IdSolicitud);
                    objAgregarCita.putExtra("fechaActualizar", fechaActualizar);
                    objAgregarCita.putExtra("horaActualizar", horaActualizar);
                    objAgregarCita.putExtra("sintomaActualizar", sintomaActualizar);

                    context.startActivity(objAgregarCita);
                }
            });
            holder2.btneliminarSP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EliminarSolicitudPaciente(IdSolicitud,expediente);
                }
            });
        }else {
            holder2.tvfechadoctor.setText(solicitudes.getDia());
            holder2.tvhoradoctor.setText(solicitudes.getHora());
            holder2.tvnombredoctor.setText(solicitudes.getSintoma());
            holder2.tvexpedientedoctor.setText(solicitudes.getEstado());
            //CAMBIAR DE COLOR
            holder2.tvfechadoctor.setTextColor(Color.parseColor("#CD0808"));
            holder2.tvhoradoctor.setTextColor(Color.parseColor("#CD0808"));
            holder2.tvnombredoctor.setTextColor(Color.parseColor("#CD0808"));
            holder2.tvexpedientedoctor.setTextColor(Color.parseColor("#CD0808"));
            //BOTONES
            holder2.btneditarSP.setVisibility(View.GONE);
            holder2.btneliminarSP.setVisibility(View.GONE);
            holder2.btnmensajeSP.setVisibility(View.VISIBLE);
            holder2.btnmensajeSP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mensajeRechazo(mensaje);
                }
            });
        }


        return row;
    }


    /* MODAL PARA Eliminar SOLICITUD   */
    private void EliminarSolicitudPaciente(final String IdSolicitud , final String expediente){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this.context);
        dialogo1.setTitle("VER EXPEDIENTE DE CONSULTA");
        dialogo1.setMessage("¿Desea ver este expediente  ");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptarEliminar(IdSolicitud,expediente);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelaEliminar();
            }
        });
        dialogo1.create().show();
    }

    public void aceptarEliminar(String IdSolicitud,final String expediente) {

        String url = "http://192.168.1.10:8080/WEBSERVICE2/webservice/WebService/API/solicitudes/delete.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("id_solicitud", IdSolicitud);


        }catch (JSONException e){
            System.out.println("ERROR AL Eliminar LA SOLICITUD: " +e.getMessage());
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Intent i = new Intent(context, HistorialSolicitudesPaciente.class);
                i.putExtra("idExpediente", expediente);
                context.startActivity(i);
                Toast.makeText(context, "Datos Eliminado Correctamente", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(context, "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);

    }

    public void cancelaEliminar() {
        Toast t=Toast.makeText(context,"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }

    //VER EL MENSAJE CUANDO SOLICITUD SEA RECHAZADA
    private void mensajeRechazo(final String mensaje ){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this.context);
        dialogo1.setTitle("Su Solicitud ha sido Rechazada");
        dialogo1.setMessage("POR :"+mensaje);
        dialogo1.setCancelable(false);
        dialogo1.setNegativeButton("CERRAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cerrar();
            }
        });
        dialogo1.create().show();
    }
    public void cerrar() {
        //Toast t=Toast.makeText(context,"Cancelado", Toast.LENGTH_SHORT);
        //t.show();
    }
}
