package com.example.clinicautec.Controlador.Doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.DAO.CitaDao;
import com.example.clinicautec.Modelo.Consulta;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistrorialConsulataDotor extends AppCompatActivity {

    ListView lv;
    TextView txvcarnethistorial;
    //ArrayList<Cita> lista;
    ArrayList<Consulta> lista;
    RecordListHistorial adaptador = null;
    CitaDao citasdao;

    ArrayList<String> lista2;
    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_histrorial_consulata_dotor);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        txvcarnethistorial = findViewById(R.id.txvcarnethistorial);
        String carnet = getIntent().getStringExtra("carnet2");
        txvcarnethistorial.setText(carnet);
        lv =(ListView)findViewById(R.id.lvhistorialdoctor);

        /* //Forma con sqlLite para llenar la lista
        citasdao = new CitaDao(getApplicationContext());
        lista = citasdao.getCitashistorial(carnet);
        adaptador = new RecordListHistorial(this, R.layout.activity_row_citas_dotor, lista);
        lv.setAdapter(adaptador);
        adaptador.notifyDataSetChanged();
        */

        listarHistorial(carnet);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void listarHistorial(String expediente){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/consulta/HistorialConsultaD.php?id="+expediente;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                lista2 = new ArrayList<String>();
                lista = new ArrayList<>();
                try{
                    JSONArray json=response.getJSONArray("data");
                    for (int i=0;i<json.length();i++){
                        Consulta consulta = new Consulta();
                        Cita c = new Cita();
                        Expediente e = new Expediente();
                        Personal p = new Personal();

                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(i);

                        consulta.setIdConsulta(jsonObject.optInt("id_consulta"));
                        consulta.setDia(jsonObject.optString("dia"));
                        consulta.setHora(jsonObject.optString("hora"));
                        e.setIdExpediente(jsonObject.optString("id_expediente"));
                        p.setNombre(jsonObject.optString("doctor"));
                        c.setExpediente(e);
                        c.setPersonal(p);
                        consulta.setCita(c);
                        lista.add(consulta);
                        //LLenar  el jsonObject para llenar la lista
                        lista2.add(jsonObject.getString("id_consulta") +" " +jsonObject.getString("dia") + " " +jsonObject.getString("hora") + " " +jsonObject.getString("id_expediente")+ " " +jsonObject.getString("doctor"));

                        adaptador = new RecordListHistorial(HistrorialConsulataDotor.this, R.layout.activity_row_citas_dotor, lista);
                        lv.setAdapter(adaptador);
                        adaptador.notifyDataSetChanged();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No hay Datos de ninguna Consulta", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }
}
