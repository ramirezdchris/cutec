package com.example.clinicautec.Controlador.Paciente;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Enfermera.AgregarCitas;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Solicitudes;
import com.example.clinicautec.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistorialSolicitudesPaciente extends AppCompatActivity {

    ListView lv;
    FloatingActionButton foatInsertarPaciente;
    ArrayList<Solicitudes> lista;
    ArrayList<String> lista2;
    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;

    RecordListSolicitudes adaptador;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial_solicitudes_paciente);

        Toolbar myToolbar = findViewById(R.id.toolBar2);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lv = findViewById(R.id.lvlistaSolicitudesPaciente);
        queue = Volley.newRequestQueue(getApplicationContext());

        final String User = getIntent().getStringExtra("idExpediente");

        foatInsertarPaciente = findViewById(R.id.foatInsertarPaciente);

        ListaSolicitudes(User);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

        foatInsertarPaciente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent objAgregarCita = new Intent(getApplicationContext(), AgregarSolicitudModulopaciente.class);
                objAgregarCita.putExtra("idExpediente", User);
                startActivity(objAgregarCita);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void ListaSolicitudes(String idexpediente){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/solicitudes/listadeSolicitud.php?id="+idexpediente;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                lista2 = new ArrayList<String>();
                lista = new ArrayList<>();
                try {
                    JSONArray json=response.getJSONArray("data");
                    for (int i=0;i<json.length();i++){
                        Solicitudes s = new Solicitudes();
                        Expediente e = new Expediente();

                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(i);
                        s.setIdSolicitud(jsonObject.optInt("id_solicitud"));
                        s.setDia(jsonObject.optString("dia"));
                        s.setHora(jsonObject.optString("hora"));
                        s.setSintoma(jsonObject.optString("sintoma"));
                        s.setEstado(jsonObject.optString("estado"));
                        s.setMensaje(jsonObject.optString("mensaje"));
                        e.setIdExpediente(jsonObject.optString("id_expediente"));
                        s.setExpediente(e);

                        lista.add(s);
                        //LLenar  el jsonObject para llenar la lista
                        lista2.add(jsonObject.getString("id_solicitud") +" " +jsonObject.getString("dia") + " " +jsonObject.getString("hora")
                                + " " +jsonObject.getString("sintoma")+ " " +jsonObject.getString("estado")
                                + " " +jsonObject.getString("id_expediente"));

                        adaptador = new RecordListSolicitudes(HistorialSolicitudesPaciente.this, R.layout.activity_row_solicitudes_m_paciente, lista);
                        lv.setAdapter(adaptador);
                        adaptador.notifyDataSetChanged();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No hay solicitudes", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }
}
