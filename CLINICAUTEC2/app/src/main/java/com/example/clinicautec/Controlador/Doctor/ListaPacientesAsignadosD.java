package com.example.clinicautec.Controlador.Doctor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaPacientesAsignadosD extends AppCompatActivity {

    ListView lv;
    ArrayList<Cita> lista;
    RecordListPaciente adaptador = null;

    //Lista para usar en los componentes de Android
    ArrayList<String> lista2;

    RequestQueue queue;
    JsonObjectRequest jsonObjectRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pacientes_asignados_d);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        queue = Volley.newRequestQueue(getApplicationContext());

        lv =(ListView)findViewById(R.id.lvpacientesasignadosD);
        String User = getIntent().getStringExtra("RolUser");
        listaExpedienteWS(User);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

    }
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()){
                case R.id.cerrar:
                    Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(objEnfermera);

                    finishAffinity();

                    return true;
                default:
                    return super.onOptionsItemSelected(item);
            }

    }

    public void listaExpedienteWS(String user){
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/expediente/read_listadoctor1.php?id="+user;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Toast.makeText(getApplicationContext(), "Se Selecciona 2", Toast.LENGTH_LONG).show();
                String dato3 = getIntent().getStringExtra("dato");
                lista2 = new ArrayList<String>();
                lista = new ArrayList<>();
                try {
                    JSONArray json=response.getJSONArray("data");
                    for (int i=0;i<json.length();i++){
                        Cita c = new Cita();
                        Expediente e = new Expediente();
                        Paciente p =new Paciente();
                        JSONObject jsonObject=null;
                        jsonObject=json.getJSONObject(i);
                        e.setIdExpediente(jsonObject.optString("Expediente"));
                        p.setNombre(jsonObject.optString("nombre"));
                        p.setApellido(jsonObject.optString("apellido"));
                        p.setTelefono(jsonObject.optString("telefono"));
                        p.setCorreo(jsonObject.optString("Correo"));
                        e.setPaciente(p);
                        c.setExpediente(e);
                        lista.add(c);
                        //LLenar  el jsonObject para llenar la lista
                        lista2.add(jsonObject.getString("Expediente") +" " +jsonObject.getString("nombre") + " " +jsonObject.getString("apellido"));

                        // System.out.println("++++++++++++ESTE ES EL VAlor");
                        //System.out.println(lista);
                        adaptador = new RecordListPaciente(ListaPacientesAsignadosD.this , R.layout.activity_row_paciente__doctor, lista,dato3);
                        lv.setAdapter(adaptador);
                        adaptador.notifyDataSetChanged();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "No Hay ningun Paciente Asignado", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(getApplicationContext(), "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(request);
    }
}
