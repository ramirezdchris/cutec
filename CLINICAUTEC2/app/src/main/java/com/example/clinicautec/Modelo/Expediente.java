package com.example.clinicautec.Modelo;

public class Expediente {

    private String idExpediente;
    private String fechaCreacion;
    private Paciente paciente;

    public Expediente() {
    }

    public Expediente(String idExpediente, String fechaCreacion, Paciente paciente) {
        this.idExpediente = idExpediente;
        this.fechaCreacion = fechaCreacion;
        this.paciente = paciente;
    }

    public Expediente(String fechaCreacion, Paciente paciente) {
        this.fechaCreacion = fechaCreacion;
        this.paciente = paciente;
    }

    public String getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(String idExpediente) {
        this.idExpediente = idExpediente;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    @Override
    public String toString() {
        return "Expediente{" +
                "idExpediente=" + idExpediente +
                ", fechaCreacion='" + fechaCreacion + '\'' +
                ", paciente=" + paciente +
                '}';
    }
}
