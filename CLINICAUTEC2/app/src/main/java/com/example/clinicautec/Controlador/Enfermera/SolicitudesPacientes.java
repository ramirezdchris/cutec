package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Solicitudes;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SolicitudesPacientes extends AppCompatActivity {

    Expediente expediente;
    Solicitudes solicitudes;


    ListView lv;

    RequestQueue queue;
    ArrayList<Solicitudes> listaSolicitudes;
    ArrayList<String> lista;

    RecordListSolicitudes adaptador;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solicitudes_pacientes);
        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        lv = findViewById(R.id.lvSolicitudesEnfermera);

        queue = Volley.newRequestQueue(getApplicationContext());
        listaSolicitudes();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void listaSolicitudes() {
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/solicitudes/listadeSolicitudEnfermera.php";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                lista = new ArrayList<>();
                listaSolicitudes = new ArrayList<>();

                try {
                    JSONArray mJsonArray = response.getJSONArray("data");
                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mJSONObject = mJsonArray.getJSONObject(i);

                        solicitudes = new Solicitudes();
                        expediente = new Expediente();


                        solicitudes.setIdSolicitud(mJSONObject.getInt("id_solicitud"));
                        solicitudes.setDia(mJSONObject.getString("dia"));
                        solicitudes.setHora(mJSONObject.getString("hora"));
                        solicitudes.setSintoma(mJSONObject.getString("sintoma"));
                        expediente.setIdExpediente(mJSONObject.getString("id_expediente"));
                        solicitudes.setExpediente(expediente);

                        listaSolicitudes.add(solicitudes);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("Error CATCH Lista Cita: " + e.getMessage());
                }

                adaptador = new RecordListSolicitudes(SolicitudesPacientes.this, R.layout.activity_row_solicitudes_m_enfermera, listaSolicitudes);
                lv.setAdapter(adaptador);
                adaptador.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error Lista Cita: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Error" + error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        queue.add(request);
    }
}
