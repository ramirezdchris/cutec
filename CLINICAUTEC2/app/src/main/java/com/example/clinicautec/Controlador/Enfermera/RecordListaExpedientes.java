package com.example.clinicautec.Controlador.Enfermera;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Controlador.Paciente.HistorialSolicitudesPaciente;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class RecordListaExpedientes extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<Expediente> recordListExpediente;

    RequestQueue queue;
    public RecordListaExpedientes(Context context, int layout,ArrayList<Expediente> recordListExpediente){
        this.context = context;
        this.layout = layout;
        this.recordListExpediente = recordListExpediente;

        queue = Volley.newRequestQueue(context);
    }

    @Override
    public int getCount() {
        return recordListExpediente.size();
    }

    @Override
    public Object getItem(int i) {
        return recordListExpediente.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class VistaRowExpedientes{
        TextView tvExpedienteEnfermera, tvFechaEnfermera, tvnombredoctor;
        Button btneditarEE,btneliminarEE;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        View row = view;
        VistaRowExpedientes holder = new VistaRowExpedientes();

        if(row==null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.tvExpedienteEnfermera = row.findViewById(R.id.tvExpedienteEnfermera);
            holder.tvFechaEnfermera = row.findViewById(R.id.tvFechaEnfermera);
            holder.tvnombredoctor = row.findViewById(R.id.tvnombredoctor);


            row.setTag(holder);
        }
        else {
            holder = (RecordListaExpedientes.VistaRowExpedientes) row.getTag();
        }

        Expediente e = recordListExpediente.get(i);
        final String IDExpediente = e.getIdExpediente();
        final String fecha = e.getFechaCreacion();
        final String PacienteID = String.valueOf(e.getPaciente().getIdPaciente());

        String nom = e.getPaciente().getNombre();
        String apell =e.getPaciente().getApellido();

        holder.tvExpedienteEnfermera.setText(e.getIdExpediente());
        holder.tvFechaEnfermera.setText(e.getFechaCreacion());
        holder.tvnombredoctor.setText(nom+" "+apell);


        return row;
    }

    /* MODAL PARA Eliminar EXPEDIENTE   */
    private void EliminarExpedienteEnfermera(final String expediente){
        AlertDialog.Builder dialogo1 = new AlertDialog.Builder(this.context);
        dialogo1.setTitle("VER EXPEDIENTE DE CONSULTA");
        dialogo1.setMessage("¿Desea ver este expediente  ");
        dialogo1.setCancelable(false);
        dialogo1.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                aceptarEliminar(expediente);
            }
        });
        dialogo1.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogo1, int id) {
                cancelaEliminar();
            }
        });
        dialogo1.create().show();
    }

    public void aceptarEliminar(final String expediente) {

        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/expediente/delete.php";

        JSONObject jsonObject = null;
        Map<String,String> params = new Hashtable<String,String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("id_expediente", expediente);


        }catch (JSONException e){
            System.out.println("ERROR AL Eliminar LA Expediente: " +e.getMessage());
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Intent i = new Intent(context, listaExpedienteEnfermera.class);
                //i.putExtra("idExpediente", expediente);
                context.startActivity(i);
                Toast.makeText(context, "Datos Eliminado Correctamente", Toast.LENGTH_LONG).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("No se pudo Conectar al Servidor" +error.toString());
                Toast.makeText(context, "No se ha podido establecer conexión con el servidor" +
                        " "+error, Toast.LENGTH_LONG).show();
            }
        }){

        };

        queue.add(request);

    }

    public void cancelaEliminar() {
        Toast t=Toast.makeText(context,"Cancelado", Toast.LENGTH_SHORT);
        t.show();
    }
}
