package com.example.clinicautec.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.Util.ConexionSQLiteHelper;

import java.util.ArrayList;

public class CitasDao {

    ConexionSQLiteHelper conn;
    SQLiteDatabase db;

    public CitasDao(Context context){
        conn = new ConexionSQLiteHelper(context, "db", null, 1);

    }

    public void openDB(){
        db = conn.getWritableDatabase(); // Abrimos la base de datos para editarla
    }

    public void closeDB(){
        conn.close();
        db.close();
    }

    public boolean insertar(Cita cita){
        try {
            openDB();
            ContentValues values = new ContentValues();
            //cita.setIdCita(0);
            //values.put("id_cita", cita.getIdCita());
            values.put("dia", cita.getDia());
            values.put("hora", cita.getHora());
            values.put("id_personal", cita.getPersonal().getIdPersonal());
            values.put("id_expediente", cita.getExpediente().getIdExpediente());
            System.out.println("AQUIIII: " +cita.getDia() +" " +cita.getHora() +" " +cita.getPersonal().getIdPersonal() +" " +cita.getExpediente().getIdExpediente());
            db.insert("cita", null, values);
            return true;
        }catch (Exception e){
            System.out.println("AQUIIII: " +cita.getDia() +" " +cita.getHora() +" " +cita.getPersonal().getIdPersonal() +" " +cita.getExpediente().getIdExpediente());
            System.err.println("Error Metodo Insertar Citas Dao" +e.getMessage());
            return false;
        }
    }

    public ArrayList<Cita> getCitas(){
        ArrayList<Cita> listaCita = new ArrayList<>();
        try {
            openDB();
            Cursor c = conn.getData("SELECT c.id_cita, c.dia, c.hora, p.nombre, exp.id_expediente, paci.nombre, paci.apellido " +
                    "FROM cita c" +
                    " INNER JOIN personal p ON p.id_personal = c.id_personal" +
                    " INNER JOIN expediente exp ON exp.id_expediente = c.id_expediente" +
                    " INNER JOIN paciente paci ON paci.id_paciente = exp.id_paciente;");

            Cursor c2 = conn.getData("SELECT * FROM cita;");

            System.out.println("LLEGOOOO4");
            System.out.println("Cantidad: " +c.getCount());
            System.out.println("Cantidad2: " +c2.getCount());
            while(c.moveToNext()){
                Cita cita = new Cita();
                Expediente expediente = new Expediente();
                Personal personal = new Personal();
                Paciente paciente = new Paciente();

                cita.setIdCita(c.getInt(0));
                cita.setDia(c.getString(1));
                cita.setHora(c.getString(2));
                personal.setNombre(c.getString(3));
                expediente.setIdExpediente(c.getString(4));
                paciente.setNombre(c.getString(5));
                paciente.setApellido(c.getString(6));



                expediente.setPaciente(paciente);
                cita.setPersonal(personal);
                cita.setExpediente(expediente);
                listaCita.add(cita);
                System.out.println("Llego AQUIIIIIIIIIIII");

            }
            return listaCita;
        }catch (Exception e){
            System.out.println("LLEGOOOO3");
            System.err.println("Error Metodo getPersonal CitasDao" +e.getMessage());
            return null;
        }
    }
}
