package com.example.clinicautec.Controlador.Enfermera;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.CustomTimePickerDialog;
import com.example.clinicautec.MainActivity;
import com.example.clinicautec.Modelo.Cita;
import com.example.clinicautec.Modelo.Expediente;
import com.example.clinicautec.Modelo.Paciente;
import com.example.clinicautec.Modelo.Personal;
import com.example.clinicautec.DAO.CitasDao;
import com.example.clinicautec.DAO.ExpedienteDao;
import com.example.clinicautec.DAO.PacienteDao;
import com.example.clinicautec.DAO.PersonalDao;
import com.example.clinicautec.DAO.UsuarioDao;
import com.example.clinicautec.Modelo.Rol;
import com.example.clinicautec.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class AgregarCitas extends AppCompatActivity {

    private EditText edtBuscarPaciente, edtExpediente, edtNombrePaciente, edtHora, edtFecha;
    private ListView listaExpedientes;
    private Spinner spnDoctores;
    private Button btnHacerCita;
    private TextView tvTitutlo;

    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adaptador;

    Cita cita;
    Expediente expediente;
    Personal personal1;
    Paciente paciente;
    Rol rol;


    UsuarioDao usuarioDao;
    CitasDao citasD;
    PacienteDao pacienteDao;
    PersonalDao personalDao;
    ExpedienteDao expedienteDao;


    ArrayList<Personal> listaDoctores;
    ArrayList<Expediente> listaExpe;

    //Lista para usar en los componentes de Android
    ArrayList<String> lista1;
    ArrayList<String> lista2;
    ArrayList<String> lista3;

    //Web Service
    RequestQueue queue;

    int idPersonalDoc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_citas);

        Toolbar myToolbar = findViewById(R.id.toolBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        queue = Volley.newRequestQueue(getApplicationContext());

        cita = new Cita();
        expediente = new Expediente();
        personal1 = new Personal();
        rol = new Rol();

        citasD = new CitasDao(getApplicationContext());
        pacienteDao = new PacienteDao(getApplicationContext());
        personalDao = new PersonalDao(getApplicationContext());
        usuarioDao = new UsuarioDao(getApplicationContext());
        expedienteDao = new ExpedienteDao(getApplicationContext());

        // Esto Tocare para llenar con el WebService
        //listaDoctores = usuarioDao.getDoctores();
        //Lista Llena listaExpe = expedienteDao.getPacientesExpediente();

        final Personal personal = new Personal();
        paciente = new Paciente();

        tvTitutlo = findViewById(R.id.textView);
        edtBuscarPaciente = findViewById(R.id.edtBuscarPacientes);
        edtExpediente = findViewById(R.id.edtExpediente);
        edtNombrePaciente = findViewById(R.id.edtNombrePaciente);
        edtHora = findViewById(R.id.edtHora);
        edtFecha = findViewById(R.id.edtFecha);
        spnDoctores = findViewById(R.id.spnDoctores);
        btnHacerCita = findViewById(R.id.btnHacerCita);
        listaExpedientes = findViewById(R.id.listaPacientes);
        //listaExpedientes.setVisibility(View.INVISIBLE);

        //Ejecutar los metodos del WebService
        obtenerExpedientes();
        obtenerDoctores();

        final int opcion = getIntent().getIntExtra("opcion", 0);
        final int idSolicitud = getIntent().getIntExtra("id_solicitud", 0);

        if (opcion == 1) {
            String id_expediente = getIntent().getStringExtra("id_expediente");
            String hora = getIntent().getStringExtra("hora");
            String fecha = getIntent().getStringExtra("fecha");
            edtExpediente.setText(id_expediente);
            edtHora.setText(hora);
            edtFecha.setText(fecha);
            edtExpediente.setTextColor(Color.parseColor("#28A530"));
            edtHora.setTextColor(Color.parseColor("#28A530"));
            edtFecha.setTextColor(Color.parseColor("#28A530"));
            edtExpediente.setEnabled(false);

            edtBuscarPaciente.setVisibility(View.INVISIBLE);
            listaExpedientes.setVisibility(View.INVISIBLE);
            edtNombrePaciente.setVisibility(View.INVISIBLE);
            //edtBuscarPaciente.setEnabled(false);
            //listaExpedientes.setEnabled(false);

        } else if (opcion == 2) {
            int idCita = getIntent().getIntExtra("id_cita", 0);
            String id_expediente = getIntent().getStringExtra("id_expediente");
            String hora = getIntent().getStringExtra("hora");
            String fecha = getIntent().getStringExtra("fecha");
            edtExpediente.setText(id_expediente);
            edtHora.setText(hora);
            edtFecha.setText(fecha);
            edtExpediente.setTextColor(Color.parseColor("#28A530"));
            edtHora.setTextColor(Color.parseColor("#28A530"));
            edtFecha.setTextColor(Color.parseColor("#28A530"));
            edtExpediente.setEnabled(false);


            cita.setIdCita(idCita);

            tvTitutlo.setText("Actualizar cita de expediente: " + id_expediente);
            edtBuscarPaciente.setVisibility(View.INVISIBLE);
            listaExpedientes.setVisibility(View.INVISIBLE);
            edtNombrePaciente.setVisibility(View.INVISIBLE);
            btnHacerCita.setText("Actualizar Cita");
        }


        edtBuscarPaciente.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listaExpedientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //String ex = parent.getItemAtPosition(position).toString();
                String ex = listaExpe.get(position).getIdExpediente();
                String nombre = listaExpe.get(position).getPaciente().getNombre();
                String apellido = listaExpe.get(position).getPaciente().getApellido();


                edtExpediente.setText(ex);
                edtNombrePaciente.setText(nombre + " " + apellido);
                edtExpediente.setTextColor(Color.parseColor("#28A530"));
                edtNombrePaciente.setTextColor(Color.parseColor("#28A530"));
                edtExpediente.setEnabled(false);
                edtNombrePaciente.setEnabled(false);
            }
        });

        spnDoctores.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    //Toast.makeText(getApplicationContext(), "Id del doctor seleccionado: " +listaDoctores.get(position - 1).getIdPersonal(), Toast.LENGTH_SHORT).show();
                    idPersonalDoc = listaDoctores.get(position - 1).getIdPersonal();
                }
                {
                    //Toast.makeText(getApplicationContext(), "Seleccione un doctor porfavor", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtHora.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mHour = c.get(Calendar.HOUR_OF_DAY);
                int mMinute = c.get(Calendar.MINUTE);


                /*CustomTimePickerDialog customTimePickerDialog = new CustomTimePickerDialog(AgregarCitas.this,R.style.DialogTheme, new CustomTimePickerDialog.OnTimeSetListener(){
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String horaFormateada = (hourOfDay < 10) ? String.valueOf("0" + hourOfDay) : String.valueOf(hourOfDay);
                        String minutoFormateado = (minute < 10) ? String.valueOf("0" + minute) : String.valueOf(minute);
                        edtHora.setText(horaFormateada +":" +minutoFormateado);
                    }
                }, mHour, mMinute, false);
                customTimePickerDialog.show();
                */

                final TimePickerDialog timePickerDialog = new TimePickerDialog(AgregarCitas.this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String horaFormateada = (hourOfDay < 10) ? String.valueOf("0" + hourOfDay) : String.valueOf(hourOfDay);
                        String minutoFormateado = (minute < 10) ? String.valueOf("0" + minute) : String.valueOf(minute);

                        int hora = Integer.parseInt(horaFormateada);
                        if (!(minutoFormateado.equals("00") || minutoFormateado.equals("30"))) {
                            //edtHora.setText(horaFormateada +":" +minutoFormateado);
                            //edtHora.setTextColor(Color.parseColor("#28A530"));
                            edtHora.setText("Intervalo de 00 y 30 minutos");
                            edtHora.setTextColor(Color.parseColor("#CF0707"));
                        } else if (!(hourOfDay > 06 && hourOfDay < 18)) {
                            edtHora.setText("Solo horarios de 7 a.m a 5:00pm");
                            edtHora.setTextColor(Color.parseColor("#CF0707"));
                        } else {
                            //edtHora.setText("Solo horarios de 7 a.m a 5:00pm");
                            //edtHora.setTextColor(Color.parseColor("#CF0707"));

                            //Toast.makeText(getApplicationContext(), "Horarios de 7:00 A.M a 5:00 p.m." , Toast.LENGTH_LONG).show();
                            edtHora.setText(horaFormateada + ":" + minutoFormateado);
                            edtHora.setTextColor(Color.parseColor("#28A530"));
                        }

                    }
                }, mHour, mMinute, false);
                timePickerDialog.show();


            }
        });

        edtFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(AgregarCitas.this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        final int mesActual = month + 1;
                        String diaFormateado = (dayOfMonth < 10) ? "0" + String.valueOf(dayOfMonth) : String.valueOf(dayOfMonth);
                        String mesFormateado = (mesActual < 10) ? "0" + String.valueOf(mesActual) : String.valueOf(mesActual);
                        edtFecha.setText(year + "-" + mesFormateado + "-" + diaFormateado);
                        edtFecha.setTextColor(Color.parseColor("#28A530"));
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(c.getTime().getTime());
                datePickerDialog.show();
            }
        });


        btnHacerCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fecha = edtFecha.getText().toString().trim();
                String hora = edtHora.getText().toString().trim();
                String exp = edtExpediente.getText().toString().trim();
                if (TextUtils.isEmpty(hora)) {
                    //.setError("Campo no puede ir vacio");
                    edtHora.setText("Campo no puede ir vacio");
                    edtHora.setTextColor(Color.parseColor("#CF0707"));
                } else if (TextUtils.isEmpty(fecha)) {
                    //edtFecha.setError("Campo no puede ir vacio");
                    edtFecha.setText("Campo no puede ir vacio");
                    edtFecha.setTextColor(Color.parseColor("#CF0707"));
                } else if (TextUtils.isEmpty(exp)) {
                    edtExpediente.setError("Campo no puede ir vacio porfavor buscar el Paciente");
                } else {

                    if (hora.equals("Hora debe ser en punto o media hora")) {
                        edtFecha.setText("Campo no puede ir vacio");
                        edtFecha.setTextColor(Color.parseColor("#CF0707"));
                    } else {
                        cita.setDia(fecha);
                        cita.setHora(hora);
                        personal.setIdPersonal(idPersonalDoc);
                        expediente.setIdExpediente(exp);
                        cita.setPersonal(personal);
                        cita.setExpediente(expediente);
                        //mostrarDialog(cita);
                        if (btnHacerCita.getText().toString().equals("Actualizar Cita")) {
                            mostrarDialog2(cita);
                        } else {
                            if (opcion == 1) {
                                aprobarSolicitud(idSolicitud);
                                mostrarDialog(cita);
                            } else {
                                mostrarDialog(cita);
                            }

                        }
                    }

                }


            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.cerrar:
                Intent objEnfermera = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(objEnfermera);

                finishAffinity();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void mostrarDialog(final Cita cita) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AgregarCitas.this);
        builder.setTitle("Agregar cita");
        builder.setMessage("¿Quieres guardar esta cita?").setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //citasD = new CitasDao(getApplicationContext());
                String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/cita/create.php";

                JSONObject jsonObject = null;
                Map<String, String> params = new Hashtable<String, String>();

                try {
                    jsonObject = new JSONObject();
                    jsonObject.put("dia", cita.getDia());
                    jsonObject.put("hora", cita.getHora());
                    jsonObject.put("estado", "Pendiente");
                    jsonObject.put("idp", cita.getPersonal().getIdPersonal());
                    jsonObject.put("idex", cita.getExpediente().getIdExpediente());

                } catch (JSONException e) {
                    System.out.println("ERRRRORRRR EN EL CATCH: " + e.getMessage());
                }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("mensaje").equals("Ya existe una cita a esa misma hora y fecha")) {
                                Toast.makeText(getApplicationContext(), response.getString("mensaje"), Toast.LENGTH_LONG).show();
                                edtHora.setText("");
                                edtFecha.setText("");
                            } else {
                                Toast.makeText(getApplicationContext(), response.getString("mensaje"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("ERRORRRRRRRRRRRRRRRRRR AL CREAR: " + error);
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
                    /*
                    @Override
                    protected Map<String ,String> getParams(){
                        Map<String, String> parametros = new HashMap<String,String>();
                        parametros.put("dia", cita.getDia());
                        parametros.put("hora", cita.getHora());
                        parametros.put("estado", cita.getEstado());
                        parametros.put("idp", String.valueOf(cita.getPersonal().getIdPersonal()));
                        parametros.put("idex", cita.getExpediente().getIdExpediente());
                        return parametros;
                    }

                    @Override
                    public Map<String ,String> getHeaders() throws AuthFailureError {
                        Map<String, String> parametros = new HashMap<String,String>();
                        parametros.put("Content-Type", "application/json");
                        return parametros;

                    }*/
                };

                queue.add(request);
                /*
                boolean insert = citasD.insertar(cita);
                if(insert){
                    Toast.makeText(getApplicationContext(), "Se guardaron", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Error al guardar", Toast.LENGTH_SHORT).show();
                }*/

            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "No se realizo la cita ", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).show();
    }

    private void mostrarDialog2(final Cita cita) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AgregarCitas.this);
        builder.setTitle("Actualizar");
        builder.setMessage("¿Quieres actualizar esta cita?").setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //citasD = new CitasDao(getApplicationContext());
                String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/cita/update.php";

                JSONObject jsonObject = null;
                Map<String, String> params = new Hashtable<String, String>();

                try {
                    jsonObject = new JSONObject();
                    jsonObject.put("id_cita", cita.getIdCita());
                    jsonObject.put("dia", cita.getDia());
                    jsonObject.put("hora", cita.getHora());
                    jsonObject.put("id_personal", cita.getPersonal().getIdPersonal());
                    jsonObject.put("id_expediente", cita.getExpediente().getIdExpediente());

                } catch (JSONException e) {
                    System.out.println("ERRRRORRRR EN EL CATCH: " + e.getMessage());
                }

                JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("mensaje").equals("Ya existe una cita a esa misma hora y fecha")) {
                                Toast.makeText(getApplicationContext(), response.getString("mensaje"), Toast.LENGTH_LONG).show();
                                edtHora.setText("");
                                edtFecha.setText("");
                            } else {
                                Toast.makeText(getApplicationContext(), response.getString("mensaje"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("ERRORRRRRRRRRRRRRRRRRR AL CREAR: " + error);
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {

                };
                queue.add(request);

            }
        }).setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(), "No se realizo la cita ", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }).show();
    }

    private void aprobarSolicitud(int idSolicitud) {

        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/cita/aprobarSolicitudEnfermera.php";

        JSONObject jsonObject = null;
        Map<String, String> params = new Hashtable<String, String>();

        try {
            jsonObject = new JSONObject();
            jsonObject.put("id_solicitud", idSolicitud);
            jsonObject.put("estado", "Aprobado");


        } catch (JSONException e) {
            System.out.println("ERRRRORRRR EN EL CATCH: " + e.getMessage());
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("ERRORRRRRRRRRRRRRRRRRR AL CREAR: " + error);
                error.printStackTrace();
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show();
            }
        }) {

        };
        queue.add(request);

    }


    private void obtenerDoctores() {

        //String url = "https://api.androidhive.info/contacts/";
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/personal/listaDoctores.php";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println("Aqui estamos 1 ");
                ArrayList listaP = new ArrayList();
                lista1 = new ArrayList<String>();
                lista1.add("Seleccione un Doctor");

                listaDoctores = new ArrayList<>();
                try {

                    JSONArray mJSONArray = response.getJSONArray("data");
                    for (int i = 0; i < mJSONArray.length(); i++) {
                        JSONObject mJSONObject = mJSONArray.getJSONObject(i);

                        personal1 = new Personal();
                        rol = new Rol();

                        personal1.setIdPersonal(mJSONObject.getInt("id_personal"));
                        personal1.setNombre(mJSONObject.getString("nombre"));
                        personal1.setApellido(mJSONObject.getString("apellido"));

                        // Para obtener las variables
                        //String name = mJSONObject.getString("nombre");
                        //lista3.add(expediente);
                        listaDoctores.add(personal1);
                        lista1.add(mJSONObject.getString("nombre") + " " + mJSONObject.getString("apellido"));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("Error: " + e.getMessage());
                    //Toast.makeText(ListadoPacientes.this, "Error: " +e.getMessage() , Toast.LENGTH_SHORT).show();
                }


                adaptador = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, lista1);
                spnDoctores.setAdapter(adaptador);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Aqui estamos 2 " + error.getMessage());
            }
        });

        queue.add(request);
    }

    private void obtenerExpedientes() {

        //String url = "http://192.168.1.11:8080/WEBSERVICE2/webservice/WebService/API/expediente/read_expedientes.php";
        String url = "https://clinicautec2020.azurewebsites.net/myapp/WebService/API/expediente/read_expedientes.php";


        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                System.out.println("Aqui estamos 1 ");
                ArrayList listaP = new ArrayList();
                lista3 = new ArrayList<String>();

                listaExpe = new ArrayList<>();
                try {

                    JSONArray mJSONArray = response.getJSONArray("data");
                    for (int i = 0; i < mJSONArray.length(); i++) {
                        JSONObject mJSONObject = mJSONArray.getJSONObject(i);

                        expediente = new Expediente();
                        paciente = new Paciente();

                        expediente.setIdExpediente(mJSONObject.getString("id_expediente"));
                        paciente.setNombre(mJSONObject.getString("nombre"));
                        paciente.setApellido(mJSONObject.getString("apellido"));
                        expediente.setPaciente(paciente);

                        // Para obtener las variables
                        //String name = mJSONObject.getString("nombre");
                        //lista3.add(expediente);
                        listaExpe.add(expediente);
                        lista3.add(mJSONObject.getString("id_expediente") + " " + mJSONObject.getString("nombre") + " " + mJSONObject.getString("apellido"));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("Error: " + e.getMessage());
                    //Toast.makeText(ListadoPacientes.this, "Error: " +e.getMessage() , Toast.LENGTH_SHORT).show();
                }

                adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, lista3);

                System.out.println("LISTA \n" + lista3);
                System.out.println("LISTA2 \n\n" + expedienteDao.getPacientesExpediente());
                listaExpedientes.setAdapter(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Aqui estamos 2 " + error.getMessage());
            }
        });

        queue.add(request);
    }
}
