package com.example.clinicautec.Controlador.Enfermera;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.clinicautec.Modelo.Solicitudes;
import com.example.clinicautec.R;

import java.util.ArrayList;

public class RecordListSolicitudes extends BaseAdapter {

    private Context context;
    private int layout;
    //private ArrayList<Cita> recordListCita;
    private ArrayList<Solicitudes> recordListSolicitudes;
    private RequestQueue queue;



    public RecordListSolicitudes(Context context, int layout,  ArrayList<Solicitudes> recordListSolicitudes){
        this.context = context;
        this.layout = layout;
        this.recordListSolicitudes = recordListSolicitudes;
        queue = Volley.newRequestQueue(context);
    }

    @Override
    public int getCount() {
        return recordListSolicitudes.size();
    }

    @Override
    public Object getItem(int position) {
        return recordListSolicitudes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imageView;
        TextView tvExpedientePacienteSoli,tvHoraSoli, tvFechaSoli, tvSintomaSoli;
        Button btnAprobarSoli, btnRechazarSoli;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordListSolicitudes.ViewHolder holder = new RecordListSolicitudes.ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);
            holder.tvExpedientePacienteSoli = row.findViewById(R.id.tvExpedientePacienteSoli);
            holder.tvFechaSoli = row.findViewById(R.id.tvFechaSoli);
            holder.tvHoraSoli = row.findViewById(R.id.tvHoraSoli);
            holder.tvSintomaSoli = row.findViewById(R.id.tvSintomaSoli);

            holder.btnAprobarSoli = row.findViewById(R.id.btnAprobarSoli);
            holder.btnRechazarSoli = row.findViewById(R.id.btnRechazarSoli);
            row.setTag(holder);
        }else{
            holder = (RecordListSolicitudes.ViewHolder)row.getTag();
        }

        final Solicitudes solicitudes = recordListSolicitudes.get(position);

        holder.tvExpedientePacienteSoli.setText(solicitudes.getExpediente().getIdExpediente());
        holder.tvHoraSoli.setText(solicitudes.getHora());
        holder.tvFechaSoli.setText(solicitudes.getDia());
        holder.tvSintomaSoli.setText(solicitudes.getSintoma());
        //holder.btnEnfeActualizar = row.findViewById(R.id.btnEnfeActualizar);
        //holder.btnEnfeEliminar = row.findViewById(R.id.btnEnfeEliminar);


        final String id_expediente = solicitudes.getExpediente().getIdExpediente();
        final String hora = solicitudes.getHora();
        final String fecha = solicitudes.getDia();


        holder.btnAprobarSoli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mostrarDialogoBasico(id_expediente, nombre, apellido, dui, carnet, telefono, fecha_nacimiento, correo, pass, id_paciente);
                //Toast.makeText(context, "Hola", Toast.LENGTH_SHORT).show();
                mostrarDialogAprobar(solicitudes);
            }
        });

        holder.btnRechazarSoli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDialogRechazar(solicitudes);
            }
        });

        return row;
    }


    private void mostrarDialogRechazar(final Solicitudes solicitudes){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this.context);

        dialog.setTitle("Rechazar solicitud");
        dialog.setMessage("¿Esta seguro de rechazar la solicitud?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent objRechazarSolicitud = new Intent(context, RechazarSolicitud.class);
                objRechazarSolicitud.putExtra("id_solicitud", solicitudes.getIdSolicitud());
                objRechazarSolicitud.putExtra("id_expediente", solicitudes.getExpediente().getIdExpediente());
                context.startActivity(objRechazarSolicitud);
            }
        });

        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.create().show();
    }

    private void mostrarDialogAprobar(final Solicitudes solicitudes){
        //int vistaa = R.layout.activity_row_paciente__doctor;
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this.context);

        dialog.setTitle("Hacer Cita");
        dialog.setMessage("¿Desea hacer una cita de esta solicitud?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent objAprobarSolicitud = new Intent(context, AgregarCitas.class);
                //objRechazarSolicitud.putExtra("id_solicitud", solicitudes.getIdSolicitud());
                objAprobarSolicitud.putExtra("id_expediente", solicitudes.getExpediente().getIdExpediente());
                objAprobarSolicitud.putExtra("hora", solicitudes.getHora());
                objAprobarSolicitud.putExtra("fecha", solicitudes.getDia());
                objAprobarSolicitud.putExtra("opcion", 1);
                context.startActivity(objAprobarSolicitud);
            }
        });

        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.create().show();
    }
}
